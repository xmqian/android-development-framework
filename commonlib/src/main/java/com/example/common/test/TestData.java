package com.example.common.test;


import com.example.common.widget.banner.BannerBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Desc： 专门用来获取测试数据的
 * <p>
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2019/8/21 15:33
 */
public class TestData {
    public static final String longText = "向江南折过花，对春风与红蜡。多情总似我，风流爱天下。人世肯相逢，醉眼万斗红沙";
    public static final String shortText = "君何归所处，静楼台，思";
    public static final String[] imageUrls = new String[]{
            "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1566799817816&di=8010225863a6b259888e629a1f3ec505&imgtype=0&src=http%3A%2F%2Fm.tuniucdn.com%2Ffb2%2Ft1%2FG1%2FM00%2FF5%2F0A%2FCii9EFbAGBiIYRv1AAB1HLdGA4YAAB2dgO53cAAAHU012_w0_h600_c0_t0.jpeg",
            "https://timgsa.baidu.com/timg?image&quality=80&size=b10000_10000&sec=1566789803&di=4fb628e572b38eaca732152016837654&src=http://imgs.qqzhiu.com/ViewPortFile/1/20160326/20160326021725479.jpg",
            "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1566799921475&di=ce8b714d3416f7575a10c1a6f75768dc&imgtype=0&src=http%3A%2F%2Fb.hiphotos.baidu.com%2Fimage%2Fpic%2Fitem%2F908fa0ec08fa513db777cf78376d55fbb3fbd9b3.jpg"};
    public static final String headImage = "http://5b0988e595225.cdn.sohucs.com/images/20171229/f2c51645c6ab44788b4bb69c80e13765.jpeg";
    public static final String[] imageHeads = new String[]{
            "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1597412542151&di=71392f40a610d648dd65496a339cab22&imgtype=0&src=http%3A%2F%2Fb-ssl.duitang.com%2Fuploads%2Fitem%2F201712%2F23%2F20171223144650_utARV.jpeg",
            "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1597412542151&di=a1c47a7d32442cbccc575de17c32df3a&imgtype=0&src=http%3A%2F%2F5b0988e595225.cdn.sohucs.com%2Fq_70%2Cc_zoom%2Cw_640%2Fimages%2F20180403%2F6174e9d295264fd597fdfe7996de16f6.jpeg",
            "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1597412359594&di=86eeb6faae4159f9c7905e321ae339ff&imgtype=0&src=http%3A%2F%2Fwww.chinadaily.com.cn%2Fdfpd%2Fretu%2Fattachement%2Fjpg%2Fsite1%2F20130807%2F0013729c02ba136c278463.jpg",
            "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1566799817816&di=8010225863a6b259888e629a1f3ec505&imgtype=0&src=http%3A%2F%2Fm.tuniucdn.com%2Ffb2%2Ft1%2FG1%2FM00%2FF5%2F0A%2FCii9EFbAGBiIYRv1AAB1HLdGA4YAAB2dgO53cAAAHU012_w0_h600_c0_t0.jpeg",
            "https://timgsa.baidu.com/timg?image&quality=80&size=b10000_10000&sec=1566789803&di=4fb628e572b38eaca732152016837654&src=http://imgs.qqzhiu.com/ViewPortFile/1/20160326/20160326021725479.jpg",
            "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1566799921475&di=ce8b714d3416f7575a10c1a6f75768dc&imgtype=0&src=http%3A%2F%2Fb.hiphotos.baidu.com%2Fimage%2Fpic%2Fitem%2F908fa0ec08fa513db777cf78376d55fbb3fbd9b3.jpg"
    };

    public static List<String> getImages(int size) {
        List<String> strings = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            strings.add(imageUrls[size % 3]);
        }
        return strings;
    }

    public static List getListSize(int size) {
        List list = new ArrayList();
        for (int i = 0; i < size; i++) {
            list.add("这是第" + i + "个文字条目");
        }
        return list;
    }

    public static List<BannerBean> getBanners() {
        List<BannerBean> bannerBeans = new ArrayList<>();
        bannerBeans.add(new BannerBean(imageUrls[0]));
        bannerBeans.add(new BannerBean(imageUrls[1]));
        bannerBeans.add(new BannerBean(imageUrls[2]));
        return bannerBeans;
    }
}
