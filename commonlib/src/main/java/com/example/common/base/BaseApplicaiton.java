package com.example.common.base;

import android.app.Application;
import android.content.Context;

import androidx.multidex.MultiDex;

/**
 * Desc： 父类，进行一些必要的初始化
 *
 * @author xmqian
 * Email:xmqian93@163.com
 * Date: 2020/8/12 0012 19:49
 */
public class BaseApplicaiton extends Application {
    /**
     * 系统上下文
     */
    private static Context mAppContext;

    /**
     * 获取系统上下文
     */
    public static Context getInstance() {
        return mAppContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (mAppContext == null) {
            mAppContext = this;
        }
        //分包加载
        MultiDex.install(this);
    }
}
