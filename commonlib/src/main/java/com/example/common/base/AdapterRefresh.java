package com.example.common.base;


import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.coszero.utilslibrary.app.AppManager;
import com.coszero.utilslibrary.app.AppUtils;
import com.coszero.utilslibrary.base.BaseRecyclerAdapter;
import com.coszero.utilslibrary.base.refresh.AdapterRefreshInterface;
import com.coszero.utilslibrary.utils.LogX;
import com.coszero.utilslibrary.utils.ToastUtils;
import com.example.common.config.Constant;
import com.example.commonlib.R;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by FuJinhu on 2017-1-23.
 * 统一样式的刷新,配合ListView
 */
public class AdapterRefresh<T> implements AdapterRefreshInterface<T> {
    private boolean isLoadMore;
    private boolean isRefresh;
    private int rows;
    private SmartRefreshLayout moreRecyclerView;
    private RecyclerView.Adapter adapter;
    private int imageRes = 0;//空布局图标
    private String emptyStr = "";//空布局消息
    private int firstPageCode = 1;

    /**
     * @param isRefresh 下拉刷新
     * @param isLoadMore 加载更多
     * @param rows
     */
    public AdapterRefresh(boolean isRefresh, boolean isLoadMore, int rows) {
        this.isLoadMore = isLoadMore;
        this.isRefresh = isRefresh;
        this.rows = rows;
    }

    public AdapterRefresh(boolean isRefresh, boolean isLoadMore) {
        this(isRefresh, isLoadMore, Constant.ROWS);
    }

    public void init(SmartRefreshLayout moreRecyclerView, RecyclerView.Adapter adapterBase) {
        this.moreRecyclerView = moreRecyclerView;
        this.adapter = adapterBase;
        moreRecyclerView.setEnableRefresh(isRefresh);
        moreRecyclerView.setEnableLoadMore(isLoadMore);
    }

    /**
     * @param firstPageCode 设置首页页码，默认为0
     */
    public void setFirstPageCode(int firstPageCode) {
        this.firstPageCode = firstPageCode;
    }

    @Override
    public void doRefresh(List<T> list, int page) {
        if (null != list) {
            if (list.size() >= rows && isLoadMore) {
                moreRecyclerView.setEnableLoadMore(true);
            } else {
                moreRecyclerView.setEnableLoadMore(false);
            }
            if (page <= firstPageCode) {
                if (list.size() == 0) {
//                    AppUtil.showToast(R.string.toast_nodata);
                }
                if (adapter instanceof BaseQuickAdapter) {
                    ((BaseQuickAdapter) adapter).replaceData(list);
                }
                if (adapter instanceof BaseRecyclerAdapter) {
                    ((BaseRecyclerAdapter) adapter).refresh(list);
                }
            } else {
                if (list.size() > 0) {
                    if (adapter instanceof BaseQuickAdapter) {
                        ((BaseQuickAdapter) adapter).addData(list);
                    }
                    if (adapter instanceof BaseRecyclerAdapter) {
                        ((BaseRecyclerAdapter) adapter).addAll(list);
                    }
                } else {
                    ToastUtils.showMsg(AppUtils.getString(AppManager.getAppManager().currentActivity(), R.string.text_no_more_data));
                }
            }
        } else {
            if (page == firstPageCode) {
                if (adapter instanceof BaseQuickAdapter) {
                    ((BaseQuickAdapter) adapter).replaceData(new ArrayList());
                }
                if (adapter instanceof BaseRecyclerAdapter) {
                    ((BaseRecyclerAdapter) adapter).refresh(new ArrayList());
                }
            }
//            AppUtil.showToast(R.string.toast_nodata);
            LogX.e("###", "暂无数据");
        }
    }

    @Override
    public void complete() {
        moreRecyclerView.finishRefresh();
        moreRecyclerView.finishLoadMore(1);
    }

    public void refresh() {
        moreRecyclerView.autoRefresh();
    }
}
