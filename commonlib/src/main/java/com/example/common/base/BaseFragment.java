package com.example.common.base;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.coszero.utilslibrary.utils.LogX;
import com.example.common.base.mvp.BasePresenter;
import com.example.common.widget.MyToolBar;
import com.example.commonlib.R;
import com.example.commonlib.R2;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Desc：
 *
 * @author xmqian
 * Email:xmqian93@163.com
 * Date: 2020/6/9 14:37
 */
public abstract class BaseFragment extends Fragment {
    @Nullable
    @BindView(R2.id.my_toolbar)
    MyToolBar mMyToolbar;
    protected String TAG = this.getClass().getSimpleName();
    protected AppCompatActivity mActivity;
    protected Context mContext;
    /**
     * 用来重置绑定
     */
    private Unbinder unbinder;
    private boolean isDataLoaded;

    /**
     * 是否已经初始化成功数据
     *
     * @param dataLoaded true 不再触发懒加载 false触发懒加载
     */
    public void setDataLoaded(boolean dataLoaded) {
        isDataLoaded = dataLoaded;
    }

    // <editor-fold desc="生命周期" defaultstate="collapsed">

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mActivity = (AppCompatActivity) getActivity();
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            View layout = inflater.inflate(getLayoutResource(), container, false);
            unbinder = ButterKnife.bind(this, layout);
            return layout;
        } catch (Exception e) {
            String errorMsg = "布局使用异常，移除类：" + this.getClass().getSimpleName() + " 异常布局ID:" + getLayoutResource() +
                    " 异常信息：" + e.toString();
            LogX.e("###BaseMyFragment", errorMsg);
            new Throwable(errorMsg);
        }
        return null;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            initView();
            initPresenter();
            initData();
        } catch (Exception e) {
            LogX.e("### BaseFragment", "初始化失败:" + e.getMessage());
        }
    }

    @Override
    public void onDestroyView() {
        if (unbinder != null) {
            unbinder.unbind();
        }
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        super.onDestroyView();
    }

    //Pager会调用的
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        getLazyData();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            getLazyData();
        }
    }
    //</editor-fold>


    public void getLazyData() {
        if (getUserVisibleHint() && getView() != null) {
            if (!isDataLoaded) {
                LogX.d("###", "懒加载");
                lazyLoadData();
                isDataLoaded = true;
            }
        }
    }

    public boolean isDataLoaded() {
        return isDataLoaded;
    }


    protected void setTitle(int resId) {
        setTitle(getResources().getString(resId));
    }

    protected void setTitle(String text) {
        if (getmMyToolbar() != null) {
            mMyToolbar.setMainTitle(text);
        }
    }

    @Nullable
    public MyToolBar getmMyToolbar() {
        return mMyToolbar;
    }

    /**
     * 返回布局ID
     *
     * @return 布局id
     */
    public abstract int getLayoutResource();

    protected abstract void initView();


    /**
     * 提供给MVP模式使用
     */
    protected void initPresenter() {
    }

    protected abstract void initData();

    /*懒加载封装方法*/
    public abstract void lazyLoadData();

}
