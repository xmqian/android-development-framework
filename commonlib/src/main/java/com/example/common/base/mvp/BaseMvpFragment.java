package com.example.common.base.mvp;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.View;

import com.example.common.base.BaseFragment;

/**
 * Desc： MVP基类
 *
 * @author xmqian
 * Email:xmqian93@163.com
 * Date: 2020/8/12 0012 15:02
 */
public abstract class BaseMvpFragment<P extends BasePresenter> extends BaseFragment {

    private Dialog mLoadingDialog;
    private P mPresenter;

    private boolean isDataLoaded = false;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        if (mPresenter != null) {
            mPresenter.detach();
        }
        super.onDestroyView();
    }

    @Override
    protected void initPresenter() {
        if (mPresenter == null) {
            mPresenter = getPresenter();
            mLoadingDialog = createProgressDialog();
        }
    }

    protected Dialog createProgressDialog() {
        ProgressDialog dialog = new ProgressDialog(getContext());
        dialog.setTitle("请稍等");
        dialog.setCancelable(false);
        dialog.setIndeterminate(true);
        return dialog;
    }

    public void showLoadingDialog() {
        if (mLoadingDialog != null && !mLoadingDialog.isShowing())
            mLoadingDialog.show();
    }

    public void hideLoadingDialog() {
        if (mLoadingDialog != null && mLoadingDialog.isShowing())
            mLoadingDialog.dismiss();
    }

    protected abstract P getPresenter();
}
