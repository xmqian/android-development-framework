package com.example.common.base.mvp;

/**
 * Desc： 网络请求基础model
 * <p>
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2020/5/11 17:41
 */
public class BaseModel<T> {
    private String code;
    private String messge;
    private T data;

    public String getCode() {
        return code;
    }

    public String getMessge() {
        return messge;
    }

    public T getData() {
        return data;
    }
}
