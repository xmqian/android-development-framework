package com.example.common.base.mvp;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.example.common.base.BaseActivity;
import com.example.common.dialog.loading.LoadingDialogHelper;

/**
 * Desc：
 *
 * @author xmqian
 * Email:xmqian93@163.com
 * Date: 2020/8/12 0012 15:01
 */
public abstract class BaseMvpActivity<P extends BasePresenter> extends BaseActivity {

    protected P mPresenter;
    //加载时需要的dialog
    private Dialog mLoadingDialog;

    // <editor-fold desc="生命周期" defaultstate="collapsed">
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        if (mPresenter != null) {
            mPresenter.detach();
        }
        LoadingDialogHelper.get().destroy();
        super.onDestroy();
    }
//</editor-fold>

    protected Dialog createProgressDialog() {
        ProgressDialog dialog = new ProgressDialog(this);
        dialog.setTitle("请稍等");
        dialog.setCancelable(false);
        dialog.setIndeterminate(true);
        return dialog;
    }

    @Override
    protected void initPresenter() {
        if (mPresenter == null) {
            mPresenter = getPresenter();
            mLoadingDialog = createProgressDialog();
        }
    }

    public void showLoadingDialog() {
        runOnUiThread(() -> {
            if (mLoadingDialog != null && !mLoadingDialog.isShowing()) {
                mLoadingDialog.show();
            }
        });

    }

    public void hideLoadingDialog() {
        runOnUiThread(() -> {
            if (mLoadingDialog != null && mLoadingDialog.isShowing()) {
                mLoadingDialog.dismiss();
            }
        });

    }

    protected abstract P getPresenter();
}
