package com.example.common.network;

/**
 * Desc： 网络配置
 * <p>
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2020/5/11 11:46
 */
public interface NetWorkConfig {
    /**
     * 请求超时时间
     */
    int REQ_TIMEOUT_SECOND = 10;
    /**
     * 读取数据超时时间
     */
    int REQ_READ_TIMEOUT_SECOND = 60;
    /**
     * 网络请求最大重试次数
     * 配合拦截器使用
     */
    int REQ_MAX_RETRY_NUM = 3;

}
