package com.example.common.network;

/**
 * Desc： 网络请求错误码
 * <p>
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2020/5/11 13:38
 */
public interface HttpErrorCode {
    /*统一配置code和messge参数*/
    String CODE = "code";
    String MESSGE = "message";
    /*请求成功*/
    String SUCCESS_CODE = "0";
    /*退出登录|异地登录*/
    String LOGINOUT = "1";
    /*错误提示*/
    String ERROR_MSG = "404";
}
