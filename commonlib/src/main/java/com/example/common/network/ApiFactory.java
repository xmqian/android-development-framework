package com.example.common.network;


import com.coszero.utilslibrary.app.ApkInfoUtils;
import com.coszero.utilslibrary.app.AppManager;

/**
 * Desc： 网络Api相关配置文件
 * <p>
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2019/5/17 14:50
 */
public class ApiFactory {
    public static final String CLIENT = "android";
    public static final String PACKAGE = ApkInfoUtils.getPackageName(AppManager.getAppManager().currentActivity());
    public static final String VER = ApkInfoUtils.getVersionName(AppManager.getAppManager().currentActivity());
    public static final String BASE_URL = "BuildConfig.API_HOST";
    public static final String SIGN_OUT = "center/Member/layout";
}
