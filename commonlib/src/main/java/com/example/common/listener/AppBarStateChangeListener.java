package com.example.common.listener;


import com.google.android.material.appbar.AppBarLayout;

/**
 * AppBarLayout折叠监听
 *
 * @author xmqian
 * @date 2018/7/26 0026
 * @desc AppBar折叠状态栏监听
 */
public abstract class AppBarStateChangeListener implements AppBarLayout.OnOffsetChangedListener {
    //展开，折叠，中间状态
    public enum State {
        EXPANDED, COLLAPSED, IDLE
    }

    private State mCurrentState = State.IDLE;

    @Override
    public final void onOffsetChanged(AppBarLayout appBarLayout, int i) {
//        LogUtil.d("AppBarStateChangeListener", "#### 滑动改变数值: " + i);
        onStateChangedValue(appBarLayout, Math.abs(i));
        if (i == 0) {
            if (mCurrentState != State.EXPANDED) {
                onStateChanged(appBarLayout, State.EXPANDED);
            }
            mCurrentState = State.EXPANDED;
        } else if (Math.abs(i) >= appBarLayout.getTotalScrollRange()) {
            if (mCurrentState != State.COLLAPSED) {
                onStateChanged(appBarLayout, State.COLLAPSED);
            }
            mCurrentState = State.COLLAPSED;
        } else {
            if (mCurrentState != State.IDLE) {
                onStateChanged(appBarLayout, State.IDLE);
            }
            mCurrentState = State.IDLE;
        }
    }

    public abstract void onStateChanged(AppBarLayout appBarLayout, State state);

    /**
     * @param appBarLayout
     * @param scrollValue 滚动数值，当==appBarLayout.getTotalScrollRange()为折叠
     */
    public void onStateChangedValue(AppBarLayout appBarLayout, int scrollValue) {

    }
}
