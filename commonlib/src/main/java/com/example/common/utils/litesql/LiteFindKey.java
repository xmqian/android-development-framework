package com.example.common.utils.litesql;

/**
 * Desc： 数据库查询字段
 *
 * @author xmqian
 * Email:xmqian93@163.com
 * Date: 2020/9/9 0009 13:56
 */
public interface LiteFindKey {
    /**
     * 数据库的自增id
     */
    String ID = "ID";
}
