package com.example.common.utils;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

import com.coszero.utilslibrary.app.AppUtils;
import com.coszero.utilslibrary.decoration.HorizontalDividerItemDecoration;
import com.coszero.utilslibrary.decoration.SpacingItemDecoration;
import com.coszero.utilslibrary.decoration.VerticalDividerItemDecoration;
import com.coszero.utilslibrary.utils.DensityUtil;
import com.example.commonlib.R;

/**
 * Desc： RecyclerView 分割线
 * <p>
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2019/10/28 11:55
 * @version 1
 */
public class RecyclerDiverUtils {
    /**
     * 默认分割线
     *
     * @param context
     * @return
     */
    public static RecyclerView.ItemDecoration hDiver(Context context) {
        return new HorizontalDividerItemDecoration.Builder(context).color(AppUtils.getColor(context, R.color.com_list_line)).size(1).build();
    }

    public static RecyclerView.ItemDecoration hDiverMagin(Context context) {
        int i = DensityUtil.dip2px(context, 16);
        return new HorizontalDividerItemDecoration.Builder(context).color(AppUtils.getColor(context, R.color.com_list_line)).size(1)
            .margin(i, i).build();
    }

    public static RecyclerView.ItemDecoration vDiver(Context context) {
        return new VerticalDividerItemDecoration.Builder(context).color(R.color.com_list_line).size(DensityUtil.dip2px(context, 8)).build();
    }

    public static RecyclerView.ItemDecoration vDiver(Context context, int color) {
        return new VerticalDividerItemDecoration.Builder(context).color(color).size((int) context.getResources().getDimension(R.dimen.dp_8)).build();
    }

    /**
     * 横向分割线
     *
     * @param context
     * @param color
     * @param lineSize
     * @return
     */
    public static RecyclerView.ItemDecoration hDiver(Context context, int color, int lineSize) {
        return new HorizontalDividerItemDecoration.Builder(context).color(color).size(lineSize).build();
    }

    /**
     * 空白的分割线,已背景为准
     *
     * @param context
     * @param lineSize
     * @return
     */
    public static RecyclerView.ItemDecoration hSpaceDiver(Context context, int lineSize) {
        return new SpacingItemDecoration(1, DensityUtil.dip2px(context, lineSize), false);
    }

}
