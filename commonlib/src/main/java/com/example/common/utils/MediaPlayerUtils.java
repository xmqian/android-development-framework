package com.example.common.utils;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.util.Log;

/**
 * Desc： 音频播放工具类
 *
 * @author xmqian
 * Email:xmqian93@163.com
 * Date: 2020/9/3 0003 13:49
 * @version 1
 */
public class MediaPlayerUtils {
    static MediaPlayer mMediaPlayer;
    /**
     * 简短音频播放
     */
    private static SoundPool soundPool = null;


    public static MediaPlayer getMediaPlayer() {
        if (null == mMediaPlayer) {
            mMediaPlayer = new MediaPlayer();
        }

        return mMediaPlayer;
    }


    /**
     * 播放音频
     */
    public static void playAudio(Context mContext, String fileName) {
        try {
            stopAudio();//如果正在播放就停止
            AssetManager assetManager = mContext.getAssets();
            AssetFileDescriptor afd = assetManager.openFd(fileName);
            MediaPlayer mediaPlayer = getMediaPlayer();
            mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            mediaPlayer.setLooping(false);//循环播放
            mediaPlayer.prepare();
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    mediaPlayer.start();
                }
            });
        } catch (Exception e) {
            Log.e("播放音频失败", "");
        }
    }

    public static void playAdudio(Context mContext, int rawId) {
        if (null != mMediaPlayer) {
            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.pause();
            }
            mMediaPlayer.release();
        }
        mMediaPlayer = MediaPlayer.create(mContext, rawId);
        mMediaPlayer.start();
    }


    /**
     * 停止播放音频
     */
    public static void stopAudio() {
        try {
            if (null != mMediaPlayer) {
                if (mMediaPlayer.isPlaying()) {
                    mMediaPlayer.pause();
                    mMediaPlayer.reset();
                    mMediaPlayer.stop();
                }
            }
        } catch (Exception e) {
            Log.e("stopAudio", e.getMessage());
        }
    }

    public static void PlaySound(Context c, int rawId) {
        soundPool = getSoundPool();
        if (soundPool == null)
            return;
        int id = soundPool.load(c, rawId, 1);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //第一个参数为id
        //第二个和第三个参数为左右声道的音量控制
        //第四个参数为优先级，由于只有这一个声音，因此优先级在这里并不重要
        //第五个参数为是否循环播放，0为不循环，-1为循环
        //最后一个参数为播放比率，从0.5到2，一般为1，表示正常播放。
        soundPool.play(id, 1, 1, 0, 0, 1);
    }

    private static SoundPool getSoundPool() {
        if (soundPool == null) {
            soundPool = new SoundPool(10, AudioManager.STREAM_SYSTEM, 5);
        }
        return soundPool;
    }
}
