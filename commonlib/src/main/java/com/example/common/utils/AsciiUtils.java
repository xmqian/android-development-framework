package com.example.common.utils;

import com.coszero.utilslibrary.utils.LogX;
import com.coszero.utilslibrary.utils.StringUtils;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * Desc： ASCII码转换
 *
 * @author xmqian
 * Email:xmqian93@163.com
 * Date: 2020/9/8 0008 12:02
 * A-Z 65-90
 * a-z 97-122
 * 0-9 48-57
 * @version 1
 */
public class AsciiUtils {
    /**
     * 字符串转换为Ascii
     *
     * @param value value
     * @return
     */
    public static String stringToAscii(String value) {
        StringBuffer sbu = new StringBuffer();
        char[] chars = value.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if (i != chars.length - 1) {
                sbu.append((int) chars[i]).append(",");
            } else {
                sbu.append((int) chars[i]);
            }
        }
        return sbu.toString();
    }

    /**
     * Ascii转换为字符串
     *
     * @param value
     * @return
     */
    public static String asciiToString(String value) {
        StringBuffer sbu = new StringBuffer();
        String[] chars = value.split(",");
        for (int i = 0; i < chars.length; i++) {
            sbu.append((char) Integer.parseInt(chars[i]));
        }
        return sbu.toString();
    }

    /**
     * @param ascDatas ascii码的list
     * @return 返回转化拼接后的字符串
     */
    public static String asciiToString(List<String> ascDatas) {
        if (ascDatas.size() > 0) {
            StringBuilder sbu = new StringBuilder();
            for (int i = 0; i < ascDatas.size(); i++) {
                sbu.append((char) Integer.parseInt(ascDatas.get(i)));
            }
            return sbu.toString();
        }
        return "";
    }

    /**
     * @param s
     * @return 返回16进制转化的字节数组
     * 使用方法
     * 用String包装类解析，并设置字符编码格式
     * new String(AsciiUtils.hexStringToByteArray(dataStr), "GBK");
     */
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    /**
     * @param builder 字符串
     * @return 返回转码为gb2312的16进制字符串
     */
    public static String[] stringToGb2312Hex(String builder) {
        try {
            if (!StringUtils.isEmpty(builder)) {
                String utf8 = new String(builder.getBytes(StandardCharsets.UTF_8));
                String unicode = new String(utf8.getBytes(), StandardCharsets.UTF_8);
                byte[] gb2312s = unicode.getBytes("gb2312");
                String[] gb2312HexArray = new String[gb2312s.length];
                for (int i = 0; i < gb2312s.length; i++) {
                    String hex = Integer.toHexString(DataConversion.byteToDec(gb2312s[i]));
                    gb2312HexArray[i] = hex;
                }
                return gb2312HexArray;
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            LogX.e("stringToGb2312Hex", "字符转码错误");
        }
        return new String[]{};
    }
}

