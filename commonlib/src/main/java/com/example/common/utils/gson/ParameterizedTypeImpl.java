package com.example.common.utils.gson;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * 解析泛型list
 * public static <T> Result<List<T>> fromJsonArray(String jsonStr, Class<T> clazz) {
 * // 生成List<T> 中的 List<T>
 * Type listType = new ParameterizedTypeImpl(List.class, new Class[]{clazz});
 * // 根据List<T>生成完整的Result<List<T>>
 * Type type = new ParameterizedTypeImpl(Result.class, new Type[]{listType});
 * return parseGson(jsonStr, type);
 * }
 * <p>
 * 解析Object泛型
 * public static <T> Result<T> fromJsonObject(String jsonStr, Class<T> clazz) {
 * Type type = new ParameterizedTypeImpl(Result.class, new Class[]{clazz});
 * return parseGson(jsonStr, type);
 * }
 * <p>
 * Desc：Gson泛型封装
 *
 * @author xmqian
 * Email:xmqian93@163.com
 * Date: 2020/6/10 16:26
 * @link(https://www.jianshu.com/p/d62c2be60617)
 */
public class ParameterizedTypeImpl implements ParameterizedType {
    private final Class raw;
    private final Type[] args;

    public ParameterizedTypeImpl(Class raw, Type[] args) {
        this.raw = raw;
        this.args = args;
    }

    @Override
    public Type[] getActualTypeArguments() {
        // 返回Map<String,User>里的String和User，所以这里返回[String.class,User.clas]
        return args;
    }

    @Override
    public Type getRawType() {
        // Map<String,User>里的Map,所以返回值是Map.class
        return raw;
    }

    @Override
    public Type getOwnerType() {
        // 用于这个泛型上中包含了内部类的情况,一般返回null
        return null;
    }
}
