package com.example.common.utils.glide;


import android.content.Context;

import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.Registry;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.engine.cache.LruResourceCache;
import com.bumptech.glide.module.AppGlideModule;
import com.bumptech.glide.request.RequestOptions;

import java.io.InputStream;

/**
 * Desc:在GlideV3中使用RGB_565,在GlideV4中使用的是ARGB_8888，
 * 图片质量变高了，但内存使用也增加了。要想改回RGB_565，
 * 在AppGlideModule 中应用一个 RequestOption
 * <p>
 * Author xmqian
 * Email:xmqian93@163.com
 * Date: 2019/5/17
 */
@GlideModule
public class MyGlideModule extends AppGlideModule {
    @Override
    public void applyOptions(Context context, GlideBuilder builder) {
        builder.setDefaultRequestOptions(new RequestOptions().format(DecodeFormat.PREFER_RGB_565));
        //重新设置内存限制
//        builder.setMemoryCache(new LruResourceCache(10*1024*1024));
    }


    /**
     * 清单解析的开启
     *
     * 这里不开启，避免添加相同的modules两次
     * @return
     */
    /*@Override
    public boolean isManifestParsingEnabled() {
        return false;
    }*/
}
