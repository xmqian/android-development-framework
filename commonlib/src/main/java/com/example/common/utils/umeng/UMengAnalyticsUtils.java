package com.example.common.utils.umeng;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.text.TextUtils;

import com.example.commonlib.BuildConfig;
import com.umeng.analytics.MobclickAgent;
import com.umeng.commonsdk.UMConfigure;
import com.umeng.umcrash.UMCrash;

import java.lang.reflect.Method;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.Locale;

/**
 * @author xmqian
 * @version 9.4.0
 * @date 2018/8/4 0004
 * @desc 友盟统计相关配置方法
 */
public class UMengAnalyticsUtils {

    /**
     * 在Application中做的初始化
     *
     * @param context context
     * @param appKey key
     * @param channelName 渠道名
     */
    public static void initUmeng(Context context, String appKey, String channelName) {
        //设置组件化的Log开关,参数: boolean 默认为false，如需查看LOG设置为true
        UMConfigure.setLogEnabled(BuildConfig.DEBUG);
        //开启调试模式（如果不开启debug运行不会上传umeng统计）
        MobclickAgent.setDebugMode(false);
        //日志加密
        UMConfigure.setEncryptEnabled(false);
        // isEnable: false-关闭错误统计功能；true-打开错误统计功能（默认打开） 9.0+版本失效
        MobclickAgent.setCatchUncaughtExceptions(!BuildConfig.DEBUG);
        //子进程是否支持自定义事件统计
        UMConfigure.setProcessEvent(false);
        //初始化配置信息参数1:上下文，不能为空
        //* 参数2:【友盟+】 AppKey
        //* 参数3:【友盟+】 Channel
        //* 参数4:设备类型，UMConfigure.DEVICE_TYPE_PHONE为手机、UMConfigure.DEVICE_TYPE_BOX为盒子，默认为手机
        //* 参数5:Push推送业务的secretPush推送业务的secret，需要集成Push功能时必须传入Push的secret，否则传空
        //新增与初始化
        UMConfigure.preInit(context, appKey, channelName);
        new Thread(new Runnable() {
            @Override
            public void run() {
                UMConfigure.init(context, appKey, channelName, UMConfigure.DEVICE_TYPE_PHONE, "");
                // 选用AUTO页面采集模式
                MobclickAgent.setPageCollectionMode(MobclickAgent.PageMode.AUTO);
            }
        }).start();
    }

    public static void init(Context context, String umKey) {
        initUmeng(context, umKey, "Android Pad");
    }

    /**
     * 在BaseActivity跟BaseFragmentActivity中的onResume加入
     * AUTO模式不需要对Activity进行检测
     *
     * @param context
     */
    public static void onResumeToActivity(Context context) {
//        MobclickAgent.onPageStart(context.getClass().getName());
//        MobclickAgent.onResume(context);
    }

    /**
     * 在BaseActivity跟BaseFragmentActivity中的onPause加入
     * AUTO模式不需要对Activity进行检测
     *
     * @param context
     */
    public static void onPauseToActivity(Context context) {
//        MobclickAgent.onPageEnd(context.getClass().getName());
//        MobclickAgent.onPause(context);
    }

    /**
     * 在BaseFragment中的onResume加入
     *
     * @param viewName 页面名称
     */
    public static void onResumeToFragment(String viewName) {
        MobclickAgent.onPageStart(viewName);
    }

    /**
     * 在BaseFragment中的onPause加入
     *
     * @param viewName 页面名称
     */
    public static void onPauseToFragment(String viewName) {
        MobclickAgent.onPageEnd(viewName);
    }

    /**
     * 账号统计
     * 在登录成功的地方调用
     *
     * @param userId 用户id
     */
    public static void onLogin(String userId) {
        MobclickAgent.onProfileSignIn(userId);
    }

    /**
     * @param Provider 账号来源。如果用户通过第三方账号登录，可以调用此接口进行统计。支持自定义，
     * 不能以下划线”_”开头，使用大写字母和数字标识，长度小于32 字节; 如果是上市
     * 公司，建议使用股票代码。
     * @param ID
     */
    public static void onLogin(String Provider, String ID) {
        MobclickAgent.onProfileSignIn(Provider, ID);
    }

    /**
     * 在退出登录的地方调用
     * 结束账号统计内容
     */
    public static void onLogout() {
        MobclickAgent.onProfileSignOff();
    }

    public static String getMac(Context context) {
        String mac = "";
        if (context == null) {
            return mac;
        }
        if (Build.VERSION.SDK_INT < 23) {
            mac = getMacBySystemInterface(context);
        } else {
            mac = getMacByJavaAPI();
            if (TextUtils.isEmpty(mac)) {
                mac = getMacBySystemInterface(context);
            }
        }
        return mac;

    }

    @TargetApi(9)
    private static String getMacByJavaAPI() {
        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                NetworkInterface netInterface = interfaces.nextElement();
                if ("wlan0".equals(netInterface.getName()) || "eth0".equals(netInterface.getName())) {
                    byte[] addr = netInterface.getHardwareAddress();
                    if (addr == null || addr.length == 0) {
                        return null;
                    }
                    StringBuilder buf = new StringBuilder();
                    for (byte b : addr) {
                        buf.append(String.format("%02X:", b));
                    }
                    if (buf.length() > 0) {
                        buf.deleteCharAt(buf.length() - 1);
                    }
                    return buf.toString().toLowerCase(Locale.getDefault());
                }
            }
        } catch (Throwable e) {
        }
        return null;
    }

    private static String getMacBySystemInterface(Context context) {
        if (context == null) {
            return "";
        }
        try {
            WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            if (checkPermission(context, Manifest.permission.ACCESS_WIFI_STATE)) {
                WifiInfo info = wifi.getConnectionInfo();
                return info.getMacAddress();
            } else {
                return "";
            }
        } catch (Throwable e) {
            return "";
        }
    }

    public static boolean checkPermission(Context context, String permission) {
        boolean result = false;
        if (context == null) {
            return result;
        }
        if (Build.VERSION.SDK_INT >= 23) {
            try {
                Class<?> clazz = Class.forName("android.content.Context");
                Method method = clazz.getMethod("checkSelfPermission", String.class);
                int rest = (Integer) method.invoke(context, permission);
                result = rest == PackageManager.PERMISSION_GRANTED;
            } catch (Throwable e) {
                result = false;
            }
        } else {
            PackageManager pm = context.getPackageManager();
            if (pm.checkPermission(permission, context.getPackageName()) == PackageManager.PERMISSION_GRANTED) {
                result = true;
            }
        }
        return result;
    }

    /**
     * 上传错误日志到友盟
     *
     * @param context
     * @param log
     */
    public static void uploadError(Context context, String log) {
        MobclickAgent.reportError(context, log);
        //9.0+新增自定义异常上报
        UMCrash.generateCustomLog(log, "###InfoLog");
    }

    public static void uploadError(Context context, Exception log) {
        MobclickAgent.reportError(context, log);
        //9.0+新增自定义异常上报
        UMCrash.generateCustomLog(log, "###ErrorLog");
    }

    /**
     * 点击事件埋点
     */
    public static void onClickEvent(Context context, String content) {
        MobclickAgent.onEvent(context, "click", content);
    }

    /**
     * 程序退出时，用于保存统计数据的API
     *
     * @param context
     */
    public static void onKillProcess(Context context) {
        MobclickAgent.onKillProcess(context);
    }
}
