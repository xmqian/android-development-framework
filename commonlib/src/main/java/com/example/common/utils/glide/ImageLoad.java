package com.example.common.utils.glide;

import android.content.Context;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.signature.ObjectKey;
import com.coszero.utilslibrary.utils.DensityUtil;
import com.coszero.utilslibrary.utils.LogX;
import com.example.commonlib.R;

import jp.wasabeef.glide.transformations.BlurTransformation;

/**
 * Desc: 图片加载工具,使用GlideV4.11版本
 * 新加图片加载方法时，如果需要用到内部静态变量mOptin,需要执行mOption.autoClone()方法来进行自定义
 * #磁盘缓存策略
 * DiskCacheStrategy.NONE 	表示不缓存任何内容。
 * DiskCacheStrategy.DATA 	表示只缓存原始图片。
 * DiskCacheStrategy.RESOURCE 	表示只缓存转换过后的图片。
 * DiskCacheStrategy.ALL 	表示既缓存原始图片，也缓存转换过后的图片。
 * DiskCacheStrategy.AUTOMATIC 	表示让Glide根据图片资源智能地选择使用哪一种缓存策略（默认选项）。
 *
 * <p>
 * Author xmqian
 * Email:xmqian93@163.com
 * Date: 2019/5/17
 */
public class ImageLoad {
    /**
     * 不使用内存缓存
     * 磁盘缓存只缓存转换后的图片
     * 加载出错默认显示灰色占位
     */
    private static RequestOptions mOption = new RequestOptions()
            .skipMemoryCache(false)
            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
            .placeholder(R.color.gray_ccc).error(R.color.gray_ccc)
            .fallback(R.color.gray_ccc).dontAnimate();

    /**
     * 正常加载图片，没有任何修饰操作
     *
     * @param context context
     * @param imageSource url
     * @param iv iv
     */
    public static <T> void loadImage(Context context, T imageSource, ImageView iv) {
        if (context == null) {
            LogX.w("####ImageLoad", "context is null");
            return;
        }
        Glide.with(context)
                .load(imageSource)
                .apply(mOption)
                .into(iv);
    }

    /**
     * 自己定义加载的配置
     *
     * @param context c
     * @param imageSource img
     * @param options 配置
     * @param iv view
     * @param <T> t
     */
    public static <T> void loadImage(Context context, T imageSource, RequestOptions options, ImageView iv) {
        if (context == null) {
            LogX.w("####ImageLoad", "context is null");
            return;
        }
        Glide.with(context)
                .load(imageSource)
                .apply(options)
                .into(iv);
    }

    /**
     * 有加载结果回调
     *
     * @param context context
     * @param imageSource image
     * @param iv iv
     * @param requestListener 图片加载回调
     * @param <T> t
     */
    public static <T> void loadImage(Context context, T imageSource, ImageView iv, @Nullable RequestListener requestListener) {
        if (context == null) {
            LogX.w("####ImageLoad", "带加载回调的 context is null");
            return;
        }
        Glide.with(context)
                .load(imageSource)
                .apply(mOption)
                .addListener(requestListener)
                .into(iv);
    }

    /**
     * 针对同一图片路径改变图片的样式
     * 针对速度做了优化，会先加载缩略图再加载原图
     *
     * @param context c
     * @param imageSource i
     * @param imageView iv
     * @param sign 签名：改变签名能使同一链接的图片保持改变
     * @param <T> t
     */
    public static <T> void loadImage(Context context, T imageSource, ImageView imageView, String sign) {
        if (context == null) {
            LogX.w("####ImageLoad", "context is null");
            return;
        }
        RequestOptions options = mOption.autoClone()
                .signature(new ObjectKey(sign));
        Glide.with(context)
                .load(imageSource)
                .thumbnail(0.1f)
                .apply(options)
                .into(imageView);
    }

    /**
     * 加载裁剪图片
     *
     * @param context c
     * @param imageSource img
     * @param isCenterCrop 是否以中心裁剪加载，否则按原图比例加载
     * @param imageView view
     * @param <T> t
     */
    public static <T> void loadImage(Context context, T imageSource, boolean isCenterCrop, ImageView imageView) {
        if (context == null) {
            LogX.w("####ImageLoad", "context is null");
            return;
        }
        RequestOptions options = mOption.autoClone();
        if (isCenterCrop) {
            options = options.centerCrop();
        } else {
            options = options.centerInside();
        }
        Glide.with(context)
                .load(imageSource)
                .apply(options)
                .into(imageView);
    }

    /**
     * 加载圆形图片
     */
    public static <T> void loadCircleImage(Context context, T imageResouce, ImageView iv) {
        if (context == null) {
            LogX.w("####ImageLoad", "context is null");
            return;
        }
        RequestOptions circleOptions = mOption.autoClone()
                .circleCrop();
        Glide.with(context)
                .load(imageResouce)
                .apply(circleOptions)
                .into(iv);

    }

    /**
     * 加载圆角图片
     * 这里使用的转换库中的代码进行转换
     *
     * @param context c
     * @param imageResouce i
     * @param roundRadius 圆角图片弧度 dp
     * @param iv view
     * @param <T>
     */
    public static <T> void loadRoundedImage(Context context, T imageResouce, int roundRadius, ImageView iv) {
        if (context == null) {
            LogX.w("####ImageLoad", "context is null");
        }
        RequestOptions blurOptions = mOption.autoClone()
                .bitmapTransform(new RoundedCorners(DensityUtil.dip2px(context, roundRadius)));
        Glide.with(context)
                .asBitmap()
                .load(imageResouce)
                .apply(blurOptions)
                .into(iv);
    }

    /**
     * 加载模糊图片
     * 只已bitmap形式加载，不会加载GIF
     * 使用内存缓存，不使用磁盘缓存
     */
    public static <T> void loadBlurImage(Context context, T url, ImageView iv) {
        if (context == null) {
            LogX.w("####ImageLoad", "context is null");
        }
        RequestOptions blurOptions = mOption.autoClone()
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
//                    .bitmapTransform(new BlurTransformation(25, 3))
                //高斯模糊 范围在 0 -- 25 越大模糊程度越高
                .transform(new BlurTransformation());
        Glide.with(context)
                .asBitmap()
                .load(url)
                .apply(blurOptions)
                .into(iv);
    }
}
