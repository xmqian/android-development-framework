package com.example.common.utils.eventbus;

import com.coszero.utilslibrary.utils.LogX;

import org.greenrobot.eventbus.EventBus;

/**
 * Desc： EventBus
 *
 * @author xmqian
 * Email:xmqian93@163.com
 * Date: 2020/6/9 11:44
 * @version org.greenrobot:eventbus:3.1.1
 */
public class EventUtils {
    private final String TAG = "### EventDelegate";

    public void registerEventBus(Object subscriber) {
        try {
            EventBus.getDefault().register(subscriber);
        } catch (Exception e) {
            LogX.e(TAG, "注册EventBus失败");
        }
    }

    public void unregisterEventBus(Object subscriber) {
        if (EventBus.getDefault().isRegistered(subscriber)) {
            EventBus.getDefault().unregister(subscriber);
        }
    }
}
