package com.example.common.widget.banner;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.coszero.uilibrary.banner.widget.Banner.BaseIndicatorBanner;
import com.example.commonlib.R;

/**
 * Desc： 不自动翻页的Banner
 * <p>
 *
 * @author ： xmqian
 * Email:xmqian93@163.com
 * Date: 2020/3/18 11:51
 * @version 1
 */
public class BannerView extends BaseIndicatorBanner<BannerBean, BannerView> {

    private int dp2;
    private int dp10;

    public BannerView(Context context) {
        this(context, null);
    }

    public BannerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BannerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        dp2 = (int) getResources().getDimension(R.dimen.dp_3);
        dp10 = (int) getResources().getDimension(R.dimen.dp_12);
    }

    @Override
    public void onTitleSlect(TextView tv, int position) {
        //设置标题
    }

    @Override
    public View onCreateItemView(int position) {
//        LogUtil.d("###","图片height:"+height+"图片宽度："+width);
        return null;
    }
}
