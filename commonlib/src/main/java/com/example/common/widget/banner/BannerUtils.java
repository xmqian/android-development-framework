package com.example.common.widget.banner;


import android.view.View;

import com.coszero.uilibrary.banner.anim.unselect.NoAnimExist;
import com.coszero.uilibrary.banner.transform.DepthTransformer;

import java.util.List;

/**
 * Desc：
 *
 * @author xmqian
 * Email:xmqian93@163.com
 * Date: 2021/7/13 0013 10:39
 * @version 1
 */
public class BannerUtils {
    /**
     * @param bannerGalleryView
     * @param bannerBeans 正确的泛型应该为：BannerBean
     */
    public static void startBanner(AutoBannerView bannerGalleryView, List<BannerBean> bannerBeans) {
        bannerGalleryView.setSelectAnimClass(NoAnimExist.class).setSource(bannerBeans).setTransformerClass(DepthTransformer.class).startScroll();
    }

    /**
     * 不自动翻页的Banner
     *
     * @param bannerGalleryView
     * @param bannerBeans
     */
    public static void startNoScrollBanner(BannerView bannerGalleryView, List<BannerBean> bannerBeans) {
        if (bannerBeans.size() != 0) {
            bannerGalleryView.setSelectAnimClass(NoAnimExist.class)
                .setSource(bannerBeans).startScroll();
        }
    }

    /**
     * 开始画廊Banner
     *
     * @param bannerGalleryView
     * @param bannerBeans
     * @param delayTime 延迟自动切换时间,秒
     */
    public static void startGllery(AutoBannerView bannerGalleryView, List<BannerBean> bannerBeans, int delayTime) {
//        bannerGalleryView.setPageMargin();
        if (bannerBeans.size() != 0) {
            bannerGalleryView.setVisibility(View.VISIBLE);
            bannerGalleryView.setSelectAnimClass(NoAnimExist.class).setSource(bannerBeans)
                .setTransformerClass(DepthTransformer.class)
                .setDelay(delayTime)
                .setPeriod(delayTime)
                .startScroll();
        }else {
            bannerGalleryView.setVisibility(View.GONE);
        }
    }
}
