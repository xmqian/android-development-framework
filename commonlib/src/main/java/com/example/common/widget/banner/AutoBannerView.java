package com.example.common.widget.banner;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.coszero.uilibrary.banner.widget.Banner.BaseIndicatorBanner;
import com.coszero.utilslibrary.device.DeviceScreenInfoUtils;
import com.coszero.utilslibrary.utils.DensityUtil;
import com.example.common.utils.glide.ImageLoad;

/**
 * Desc： 自动翻页banner
 * <p>
 *
 * @author ： xmqian
 * Email:xmqian93@163.com
 * Date: 2020/3/18 11:51
 * @version 1
 */
public class AutoBannerView extends BaseIndicatorBanner<BannerBean, AutoBannerView> {

    private int height;
    private int width;
    private int paddingBottom;
    private int dp2;

    public AutoBannerView(Context context) {
        this(context, null);
    }

    public AutoBannerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AutoBannerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        int screenWidth = DeviceScreenInfoUtils.getScreenWidth(getContext());
        paddingBottom = DensityUtil.dip2px(mContext, 30);
        dp2 = DensityUtil.dip2px(mContext, 2);
        //父布局的最大高度
        height = (int) (screenWidth * 0.5) - paddingBottom;
        //图片的最大宽度
        width = screenWidth;
    }

    @Override
    public boolean isValid() {
        return super.isValid();
    }

    @Override
    public void onTitleSlect(TextView tv, int position) {
        //设置标题
    }

    @Override
    public View onCreateItemView(int position) {
//        LogUtil.d("###","图片height:"+height+"图片宽度："+width);
        int screenWidth = DeviceScreenInfoUtils.getScreenWidth(getContext());
        BannerBean bannerBean = mDatas.get(position);
        ImageView iv = new ImageView(getContext());
        iv.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        iv.setScaleType(ImageView.ScaleType.CENTER_CROP);
        String imgUrl = bannerBean.getUrl();
//        Glide.with(mContext).load(imgUrl).into(iv);
        ImageLoad.loadImage(getContext(), imgUrl, iv);
        return iv;
    }
}
