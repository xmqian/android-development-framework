package com.example.common.widget.banner;

import com.coszero.utilslibrary.app.AppCheckUtils;

/**
 * Desc： 横幅BannerView实体
 *
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2019/9/23 10:40
 * @version 1
 */
public class BannerBean {

    /**
     * name : 广告名称
     * url : 广告图片
     * jumpUrl : 广告跳转地址
     * jumpType : 跳转类型 1是外部链接 2是app内部跳转
     * SystemClass : 系统ID
     * SystemClassVal : 系统分类
     */

    private String name;
    private String url;
    private String jumpUrl;
    private String jumpType;
    private String SystemClass;
    private String SystemClassVal;

    public BannerBean(String url) {
        this.url = url;
    }

    public BannerBean() {
    }

    public String getName() {
        return name;
    }

    public void setName(String Name) {
        this.name = Name;
    }

    public String getUrl() {
        return AppCheckUtils.checkString(url);
    }

    public void setUrl(String Pic) {
        this.url = Pic;
    }

    public String getJumpUrl() {
        return jumpUrl;
    }

    public void setJumpUrl(String Url) {
        this.jumpUrl = Url;
    }

    public String getJumpType() {
        return AppCheckUtils.checkIntStr(jumpType);
    }

    public void setJumpType(String UrlType) {
        this.jumpType = UrlType;
    }

    public String getSystemClass() {
        return SystemClass;
    }

    public void setSystemClass(String SystemClass) {
        this.SystemClass = SystemClass;
    }

    public String getSystemClassVal() {
        return SystemClassVal;
    }

    public void setSystemClassVal(String SystemClassVal) {
        this.SystemClassVal = SystemClassVal;
    }

    @Override
    public String toString() {
        return getUrl();
    }
}
