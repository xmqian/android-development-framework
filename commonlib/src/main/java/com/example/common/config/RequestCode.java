package com.example.common.config;

/**
 * Desc： 内部申请id
 *
 * @author xmqian
 * Email:xmqian93@163.com
 * Date: 2020/11/30 0030 18:22
 */
public interface RequestCode {
    /**
     * 请求安装位置来源
     */
    int REQUEST_CODE_UNKNOWN_APP = 303;
    /**
     * 权限申请
     */
    int REQUEST_PERMISSION_CODE = 101;
}
