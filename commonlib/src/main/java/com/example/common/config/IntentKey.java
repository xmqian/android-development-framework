package com.example.common.config;
/**
 * Desc： 传递数据使用的key
 *
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2020/5/11 13:46
 */
public interface IntentKey {
    String ID = "id";
    String TYPE = "type";
    String DATA = "data";
    String STATUS = "status";
    String EXTRA_TITLE = "title";
}
