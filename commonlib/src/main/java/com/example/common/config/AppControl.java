package com.example.common.config;


import com.example.commonlib.BuildConfig;

/**
 * App的功能控制
 *
 * @author xmqian
 * @date 2018/12/29 10:25
 * @desc app设置的静态常量，控制项目内的配置，比如是否为DEBUG模式,运用服务器版本
 */
public interface AppControl {
    /*是否延迟过渡动画*/
    boolean DELAY_SPLASH = false;
    /*是否进入引导页，false直接进入登录*/
    boolean INTO_GUIDE = false;
    /*是否需要先登录再使用*/
    boolean NEED_FRIST_LOGIN = false;
    /*是否为测试模式-可手控,此常量用于日志或在正式版本中绝对不执行的程序*/
    boolean ISDEBUG = BuildConfig.SHOW_LOG;
    /*是否使用测试数据,用于模拟数据或者模拟网络请求*/
    boolean USE_TEST_DATA = BuildConfig.DEBUG;
    /*是否要使用EventBus*/
    boolean USE_EVENTBUS = true;
}
