package com.example.common.config.user;

import android.content.Context;

import com.coszero.utilslibrary.app.AppManager;
import com.coszero.utilslibrary.utils.SharedPreUtils;
import com.coszero.utilslibrary.utils.StringUtils;


/**
 * Desc：用户信息管理类，用作登录
 * <p>
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2019/6/4 12:59
 */
public class UserManager {
    private static UserManager INSTANCE = null;
    private Context mContext;
    private boolean isLogin;//登录状态

    private UserManager(Context context) {
        mContext = context;
    }

    public static UserManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new UserManager(AppManager.getAppManager().currentActivity());
        }
        return INSTANCE;
    }

    public static UserManager getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new UserManager(context);
        }
        return INSTANCE;
    }

    /**
     * 初始化登录状态
     */
    public void initLoginData() {
        if (!StringUtils.isEmpty(getToken())) {
            isLogin = true;
        } else {
            isLogin = false;
            clearLoginData();
        }
    }

    public boolean isLogin() {
        return isLogin;
    }

    /**
     * 保存登录信息到本地
     */
    public void saveLoginData(String name, String headUrl, String token, String id) {
        SharedPreUtils.saveStringData(mContext, LoginConstance.USER_NICKNAME, name);
        SharedPreUtils.saveStringData(mContext, LoginConstance.USER_HEAD_IMAGE, headUrl);
        SharedPreUtils.saveStringData(mContext, LoginConstance.USER_TOKEN, token);
        SharedPreUtils.saveStringData(mContext, LoginConstance.USER_ID, id);
        isLogin = true;
    }

    public void saveLoginData(String name, String headUrl, String token) {
        saveLoginData(name, headUrl, token, "0");
    }

    public String getToken() {
        return SharedPreUtils.getStringData(mContext, LoginConstance.USER_TOKEN, "");
    }

    public String getCardId() {
        return SharedPreUtils.getStringData(mContext, LoginConstance.USER_ID, "");
    }

    public String getNickName() {
        return SharedPreUtils.getStringData(mContext, LoginConstance.USER_NICKNAME, "");
    }

    public String getHeadImg() {
        return SharedPreUtils.getStringData(mContext, LoginConstance.USER_HEAD_IMAGE, "");
    }

    /**
     * 清除本地的用户登录信息
     */
    public void clearLoginData() {
        SharedPreUtils.clear(mContext);
        isLogin = false;
    }

    /**
     * 用户需要存储的信息目录
     */
    private static class LoginConstance {
        public static final String USER_ACCOUNT = "USER_ACCOUNT";//用户账户
        public static final String USER_PASSWORD = "USER_PASSWORD";//用户密码，可不存，建议加密存储
        public static final String USER_NICKNAME = "USER_NICKNAME";//用户昵称,姓名
        public static final String USER_TOKEN = "USER_TOKEN";//用户唯一标识，用于请求
        public static final String USER_HEAD_IMAGE = "USER_HEAD_IMAGE";//用户头像
        public static final String USER_ROLES = "USER_ROLES";//用户角色，多角色登录需要
        public static final String USER_ID = "USER_ID";
    }
}
