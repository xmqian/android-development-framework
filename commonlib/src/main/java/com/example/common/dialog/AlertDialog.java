package com.example.common.dialog;

import android.content.Context;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.coszero.utilslibrary.device.DeviceScreenInfoUtils;
import com.example.commonlib.R;
import com.lxj.xpopup.core.CenterPopupView;

/**
 * Desc： 确认对话框
 *
 * @author xmqian
 * Email:xmqian93@163.com
 * Date: 2020/11/30 0030 18:24
 */
public class AlertDialog extends CenterPopupView {
    private LinearLayout lLayout_bg;
    private TextView txt_title;
    private TextView txt_msg;
    private EditText edit_msg;
    private Button btn_neg;
    private Button btn_pos;
    private ImageView img_line;
    private Display display;
    private boolean showTitle = false;
    private boolean showMsg = false;
    private boolean showEditText = false;
    private boolean showPosBtn = false;
    private boolean showNegBtn = false;
    private String title = "";
    private String msg = "";
    private String editText = "";
    private String posBtnText = "";
    private String negBtnText = "";
    private OnClickListener posListener;
    private OnClickListener negListener;

    public AlertDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        WindowManager windowManager = (WindowManager) getContext()
                .getSystemService(Context.WINDOW_SERVICE);
        display = windowManager.getDefaultDisplay();
        lLayout_bg = findViewById(R.id.lLayout_bg);
        txt_title = findViewById(R.id.txt_title);
        txt_title.setVisibility(View.GONE);
        txt_msg = findViewById(R.id.txt_msg);
        txt_msg.setVisibility(View.GONE);
        edit_msg = findViewById(R.id.edit_msg);
        edit_msg.setVisibility(View.GONE);
        btn_neg = findViewById(R.id.btn_neg);
        btn_neg.setVisibility(View.GONE);
        btn_pos = findViewById(R.id.btn_pos);
        btn_pos.setVisibility(View.GONE);
        img_line = findViewById(R.id.img_line);
        img_line.setVisibility(View.GONE);
        lLayout_bg.setLayoutParams(new FrameLayout.LayoutParams((int) (display
                .getWidth() * 0.5), /*(int) (display.getHeight() * 0.3)*/LayoutParams.WRAP_CONTENT));
        initData();
    }

    private void initData() {
        txt_title.setText(title);
        txt_msg.setText(msg);
        edit_msg.setHint(editText);
        btn_pos.setText(posBtnText);
        btn_neg.setText(negBtnText);
        btn_pos.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (posListener != null) {
                    posListener.onClick(v);
                }
                if (v.getTag() == null)
                    dialog.dismiss();
                else if (!v.getTag().equals("xx")) {
                    dialog.dismiss();
                }
            }
        });
        btn_neg.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != negListener) {
                    negListener.onClick(v);
                }
                dialog.dismiss();
            }
        });
        setLayout();
    }

    @Override
    protected int getImplLayoutId() {
        return R.layout.ui_alertdialog_view;
    }

   /* @Override
    protected int getMaxHeight() {
        return (int) (DeviceScreenInfoUtils.getScreenHeight(getContext()) * 0.3);
    }*/

    @Override
    protected int getMaxWidth() {
        return (int) (DeviceScreenInfoUtils.getScreenWidth(getContext()) * 0.5);
    }

    public AlertDialog setTitle(String title) {
        showTitle = true;
        this.title = title;
        return this;
    }

    public AlertDialog setMsg(String msg) {
        showMsg = true;
        this.msg = msg;
        return this;
    }

    public AlertDialog setEditHintMsg(String msg) {
        showEditText = true;
        this.msg = msg;
        return this;
    }

    public String getEditTextString() {
        return edit_msg.getText().toString();
    }

    public AlertDialog setPositiveButton(String text,
                                         final OnClickListener listener) {
        showPosBtn = true;
        this.posBtnText = text;
        this.posListener = listener;
        return this;
    }

    public AlertDialog setNegativeButton(String text,
                                         final OnClickListener listener) {
        showNegBtn = true;
        this.negBtnText = text;
        this.negListener = listener;
        return this;
    }

    private void setLayout() {
        if (!showTitle && !showMsg && !showEditText) {
            txt_title.setText("提示");
            txt_title.setVisibility(View.VISIBLE);
        }

        if (showTitle) {
            txt_title.setVisibility(View.VISIBLE);
        }

        if (showMsg) {
            txt_msg.setVisibility(View.VISIBLE);
        }

        if (showEditText) {
            edit_msg.setVisibility(View.VISIBLE);
        }

        if (!showPosBtn && !showNegBtn) {
            btn_pos.setText("确定");
            btn_pos.setVisibility(View.VISIBLE);
            //btn_pos.setBackgroundResource(R.drawable.alertdialog_single_selector);
            btn_pos.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        }

        if (showPosBtn && showNegBtn) {
            btn_pos.setVisibility(View.VISIBLE);
            // btn_pos.setBackgroundResource(R.drawable.ul_alertdialog_right_selector);
            btn_neg.setVisibility(View.VISIBLE);
            // btn_neg.setBackgroundResource(R.drawable.ul_alertdialog_left_selector);
            img_line.setVisibility(View.VISIBLE);
        }

        if (showPosBtn && !showNegBtn) {
            btn_pos.setVisibility(View.VISIBLE);
            // btn_pos.setBackgroundResource(R.drawable.alertdialog_single_selector);
        }

        if (!showPosBtn && showNegBtn) {
            btn_neg.setVisibility(View.VISIBLE);
            //btn_neg.setBackgroundResource(R.drawable.alertdialog_single_selector);
        }
    }
}
