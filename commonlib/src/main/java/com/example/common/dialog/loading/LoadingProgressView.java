package com.example.common.dialog.loading;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import com.example.commonlib.R;

/**
 * Desc： 网络加载进度弹窗
 *
 * @author xmqian
 * Email:xmqian93@163.com
 * Date: 2020/7/10 17:19
 */
public class LoadingProgressView extends Dialog {

    public LoadingProgressView(Context context) {
        this(context, R.style.com_lodingDialog);
    }

    public LoadingProgressView(Context context, int theme) {
        super(context, theme);
//        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(false);
        setContentView(R.layout.com_dialog_loading_progress);
    }

}