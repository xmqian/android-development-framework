package com.example.common.dialog;


import android.app.Activity;

import com.coszero.uilibrary.dialog.AlertDialog;
import com.coszero.utilslibrary.app.AppManager;


/**
 * Created by duke on 15/11/14.
 */
public class CommonDialogView {

    public static void showLoginErrorDialog(String code,String msg){
        showMsgDialog(msg, "确定", "取消", new DialogClickListener() {
            @Override
            public void doConfirm() {

            }

            @Override
            public void doCancel() {

            }
        });
    }

    public static void showMsgDialog(String msg, String posBtnStr, String negaBtnStr, DialogClickListener listener) {
        Activity activity = AppManager.getAppManager().currentActivity();
        if (activity != null && activity.isFinishing()) {
//            AppUtil.showToast("窗口已关闭");
            return;
        }
        AlertDialog alertDialog = new AlertDialog(activity);
        alertDialog.builder().setMsg(msg)
                .setPositiveButton(posBtnStr, v -> listener.doConfirm())
                .setNegativeButton(negaBtnStr, v -> listener.doCancel()).show();
    }

    public interface DialogClickListener {
        void doConfirm();

        void doCancel();
    }
}
