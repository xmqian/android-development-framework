# 公共库
> 将公用的功能抽离出来，保持一个项目最少的改动适应跟多项目,新建的项目只关注于本身逻辑与界面的开发

## 库结构
- base   可继承的公共父类,做一些必要的统一操作,在应用module时建议多继承一层，使用BaseMy前缀命名,比如BaseMyActivity,可做一些不同的定制
- config    应用的配置参数,包含账户管理类,应用功能开发控制,常量等
- dialog     通用的弹窗样式
- helper     简单易用的帮助类
- listener   一些通用的回调,或者一些封装处理的回调
- network   网络请求相关的类
- test  测试相关的类，这里的测试不是指单元测试，而是应该开发过程中的测试，比如模拟数据等
- utils 第三方依赖库的封装，这里统一为工具类
- widget    自定义控件，包括第三方的自定义控件修改，继承，重绘都算，这里通常存储需要经常修改，封装性不是很强的自定义控件

## 使用规则
- values中的屏幕适配尺寸文件需要配合插件使用,插件中设置的是设计图的尺寸,如果没有设计图没必要生成,想生成可按照1080x1920生成
- gradle中依赖在线库使用**api**使其共享


## 版本适配功能
> 适配功能名称(涉及的文件更改)
- 全面屏适配(AndroidManifest)
- 分屏下界面兼容(AndroidManifest)
- 允许明文请求及http请求(AndroidManifest,res/xml/network_security_config)
- 屏幕尺寸单位适配(res/values-sw*/dimens)
- 请求前台服务权限(AndroidManifest)


## 库接入功能
- 数据库 utils/litesql
