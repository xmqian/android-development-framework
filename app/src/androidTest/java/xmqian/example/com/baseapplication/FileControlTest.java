package xmqian.example.com.baseapplication;


import android.content.Context;
import android.util.Log;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import com.coszero.utilslibrary.file.FileControl;
import com.coszero.utilslibrary.file.FileDirPathUtils;
import com.coszero.utilslibrary.file.FileUtils;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.IOException;

@RunWith(AndroidJUnit4.class)
public class FileControlTest {

    private Context appContext;

    @Before
    public void setUp() {
        appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
    }

    @Test
    public void copyAssets() {
        //从assets文件夹中拷贝文件
        //目标文件夹
        String copyTofileDir = FileDirPathUtils.createExternalStorageDir(appContext, "copyFileDir");
        //assets中的文件名
        String assetsFileName = "doc.txt";
        try {
            FileControl.copy(appContext.getAssets(), assetsFileName, copyTofileDir + File.separator + assetsFileName, false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetAssetsData() {
        Log.i("testGetAssetsData", FileUtils.getAssetsData(appContext, "doc.txt"));
    }
}