package com.xmqian.app.project.common.testdata;

/**
 * Desc： 运行java程序
 * <p>
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2020/5/11 9:46
 */
public class TestJavaRun {
    private static int aaaa;
    private static int aabb;
    private static int aaab;
    private static int abcd;
    private static int abbb;

    public static void main(String args[]) {
        String str = "";
        for (int i = 0; i < 10000; i++) {
            str = String.format("%04d", i);
//            System.out.println(str);
            String regex = "^.*(.)\\1{3}.*$";
            String regex1 = "^(\\d)\\1{1}([0-9])\\2{1}$";
            String regex2 = "^(\\d)\\1{2}([0-9])";
            String regex3 = "^(.)*(.)\\2{2}$";
            if (str.matches(regex)) {
                aaaa++;
                System.out.println("AAAA:" + str);
            } else if (str.matches(regex1)) {
                aabb++;
                System.out.println("AABB:" + str);
            } else if (str.matches(regex2)) {
                aaab++;
                System.out.println("AAAB:" + str);
            } else if (str.matches(regex3)) {
                abbb++;
                System.out.println("ABBB:" + str);
            } else {
                abcd++;
            }
        }
        System.out.println("AAAA数量：" + aaaa + "\nAABB数量：" + aabb + "\nAAAB数量：" + aaab + "\nABCD数量：" + abcd + "\nABBB数量:" + abbb);
    }
}
