package com.xmqian.app.project.ui.main.fragment;

import android.os.Bundle;

import com.example.common.widget.banner.AutoBannerView;
import com.example.common.widget.banner.BannerUtils;
import com.xmqian.app.R;
import com.example.common.config.IntentKey;
import com.xmqian.app.project.common.base.BaseMyFragment;
import com.xmqian.app.project.common.testdata.TestData;

import butterknife.BindView;

public class MainPage1Fragment extends BaseMyFragment {
    @BindView(R.id.vp_banner)
    AutoBannerView mBranner;

    public static MainPage1Fragment newInstance(String title) {
        Bundle args = new Bundle();
        args.putString(IntentKey.EXTRA_TITLE, title);
        MainPage1Fragment fragment = new MainPage1Fragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getLayoutResource() {
        return R.layout.fragment_main_page1;
    }

    @Override
    protected void initView() {
        setTitle(getArguments().getString(IntentKey.EXTRA_TITLE));
        BannerUtils.startBanner(mBranner, TestData.getBanners());
    }

    @Override
    protected void initData() {

    }

    @Override
    public void lazyLoadData() {

    }
}
