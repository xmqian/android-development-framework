package com.xmqian.app.project.ui.user.presenter;

import com.example.common.base.mvp.BasePresenter;
import com.xmqian.app.project.common.helper.RetrofitHelper;
import com.example.common.utils.gson.GsonUtil;
import com.xmqian.app.project.ui.user.contract.LoginContract;
import com.xmqian.app.project.ui.user.model.LoginModel;

import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class LoginPresenter extends BasePresenter<LoginContract.View> implements LoginContract.Presenter {

    public LoginPresenter(LoginContract.View mView) {
        super(mView);
    }

    @Override
    public void doLogin(String username, String pwd) {
        Map<String, String> map = new HashMap<>();
        map.put("username", username);
        map.put("password", pwd);
        String s = GsonUtil.toJsonStr(map);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), s);
        addTask(RetrofitHelper.getInstance().getService().login(requestBody), resp -> {
            LoginModel data = resp.getData();
            getView().onLoginSucceed(data);
        });
    }

    /**
     * 快捷登录
     *
     * @param username
     * @param smsCode
     */
    @Override
    public void quickLogin(String username, String smsCode) {
        Map<String, String> map = new HashMap<>();
        map.put("username", username);
        map.put("smsCode", smsCode);
        String s = GsonUtil.toJsonStr(map);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), s);
        addTask(RetrofitHelper.getInstance().getService().login(requestBody), resp -> {
            LoginModel data = resp.getData();
            getView().onLoginSucceed(data);
        });
    }

    /**
     * 获取验证码
     *
     * @param type
     * @param phone
     */
    @Override
    public void getSmsCode2(int type, String phone) {
        Map<String, Object> map = new HashMap<>();
        map.put("type", type);
        map.put("phone", phone);
        String s = GsonUtil.toJsonStr(map);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"), s);
        addTask(RetrofitHelper.getInstance().getService().getMsg(requestBody), resp -> {
            getView().getCodeSuccess();
        });
    }
}
