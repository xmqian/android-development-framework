package com.xmqian.app.project.ui.common.presenter;

import com.example.common.base.mvp.BasePresenter;
import com.xmqian.app.project.common.helper.RetrofitHelper;
import com.xmqian.app.project.ui.common.contract.MyWebViewContract;
import com.xmqian.app.project.ui.common.model.MyWebViewModel;

import io.reactivex.functions.Consumer;

public class MyWebViewPresenter extends BasePresenter<MyWebViewContract.View> implements MyWebViewContract.Presenter, MyWebViewContract.View {
    public MyWebViewPresenter(MyWebViewContract.View mView) {
        super(mView);
    }

    /**
     * 获取网页地址
     *
     * @param pageId
     */
    public void getSinglePage(int pageId) {
        addTask(RetrofitHelper.getInstance().getService().getContent(1, 2), new Consumer<String>() {
            @Override
            public void accept(String s) throws Exception {

            }
        });
    }

    public void getFeedBackInfo(int pageId) {

    }

    @Override
    public void onGetSinglePageSucceed(MyWebViewModel pageBean) {

    }

    @Override
    public void onRequestFailed(String message) {

    }

    @Override
    public void onNetworkError() {

    }
}
