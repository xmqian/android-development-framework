package com.xmqian.app.project.common.base;

import com.example.common.base.BaseFragment;
import com.example.common.utils.umeng.UMengAnalyticsUtils;

/**
 * Desc： 项目自定义的父类
 *
 * @author xmqian
 * Email:xmqian93@163.com
 * Date: 2020/8/12 0012 15:13
 */
public abstract class BaseMyFragment extends BaseFragment {
    @Override
    public void onResume() {
        super.onResume();
        UMengAnalyticsUtils.onResumeToFragment(TAG);
    }

    @Override
    public void onPause() {
        UMengAnalyticsUtils.onPauseToFragment(TAG);
        super.onPause();
    }
}
