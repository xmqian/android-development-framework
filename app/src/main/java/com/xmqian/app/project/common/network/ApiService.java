package com.xmqian.app.project.common.network;


import com.example.common.base.mvp.BaseModel;
import com.xmqian.app.project.ui.user.model.LoginModel;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by xuyougen on 2018/4/11.
 */

public interface ApiService {
    //登录
    @POST("v1/login")
    Observable<BaseModel<LoginModel>> login(@Body RequestBody body);

    //获取验证码
    @POST("v1/getmsg")
    Observable<BaseModel<String>> getMsg(@Body RequestBody body);

    //    ?type=1&page=1
    @GET("satinApi")
    Observable<String> getContent(@Query("type") int type, @Query("page") int page);
}
