package com.xmqian.app.project;

import android.app.Application;

import com.coszero.utilslibrary.utils.ToastUtils;
import com.example.common.base.BaseApplicaiton;
import com.xmqian.app.R;
import com.example.common.config.Global;
import com.example.common.utils.logger.LoggerUtil;
import com.example.common.utils.umeng.UMengAnalyticsUtils;


/**
 * @author xmqian
 * @date 2018/12/28 14:29
 * @desc 自定义Application
 */
public class MyApplication extends BaseApplicaiton {

    @Override
    public void onCreate() {
        super.onCreate();
        ToastUtils.init((Application) getInstance());
        LoggerUtil.initLogger(getResources().getString(R.string.app_name));
        UMengAnalyticsUtils.init(this, Global.UM_KEY);
    }

}
