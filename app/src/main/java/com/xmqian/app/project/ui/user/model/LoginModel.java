package com.xmqian.app.project.ui.user.model;

import com.xmqian.app.project.ui.user.contract.LoginContract;

/**
 * Desc： 登录返回的用户信息
 * <p>
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2019/6/4 13:15
 */
public class LoginModel implements LoginContract.Model {
    private String userName;
    private String token;
    private String headImage;

    public LoginModel(String userName, String token, String headImage) {
        this.userName = userName;
        this.token = token;
        this.headImage = headImage;
    }

    public LoginModel() {
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getHeadImage() {
        return headImage;
    }

    public void setHeadImage(String headImage) {
        this.headImage = headImage;
    }
}
