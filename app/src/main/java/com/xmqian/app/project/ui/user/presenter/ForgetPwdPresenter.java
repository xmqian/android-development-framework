package com.xmqian.app.project.ui.user.presenter;


import com.example.common.base.mvp.BasePresenter;
import com.xmqian.app.project.ui.user.contract.ForgetPwdContract;

public class ForgetPwdPresenter extends BasePresenter<ForgetPwdContract.View> implements ForgetPwdContract.Presenter {
    public ForgetPwdPresenter(ForgetPwdContract.View mView) {
        super(mView);
    }

    public void forgetnew1(String phone, String smsCode) {
    }

    public void forgetnew2(String mobile, String pwd, String confpwd) {
    }
}
