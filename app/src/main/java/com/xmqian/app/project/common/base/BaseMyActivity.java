package com.xmqian.app.project.common.base;

import com.example.common.base.BaseActivity;
import com.example.common.utils.umeng.UMengAnalyticsUtils;

/**
 * Desc： 项目自定义的父类
 *
 * @author xmqian
 * Email:xmqian93@163.com
 * Date: 2020/8/12 0012 15:12
 */
public abstract class BaseMyActivity extends BaseActivity {

    @Override
    protected void onResume() {
        super.onResume();
        UMengAnalyticsUtils.onResumeToActivity(this);
    }

    @Override
    protected void onPause() {
        UMengAnalyticsUtils.onPauseToActivity(this);
        super.onPause();
    }
}
