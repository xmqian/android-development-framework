package com.xmqian.app.project.ui.main;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.widget.TextView;

import com.coszero.utilslibrary.utils.LogX;
import com.coszero.utilslibrary.utils.RandomUtils;
import com.coszero.utilslibrary.utils.ToastUtils;
import com.xmqian.app.R;
import com.xmqian.app.project.common.base.BaseMyActivity;
import com.xmqian.app.project.event.LoginEvent;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.OnClick;


public class MainActivity extends BaseMyActivity {
    private static final long DELAY_TIME = 1500;//退出间隔时间
    private long lastPressBackKeyTime = 0;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.tv_go)
    TextView tvGo;
    private Handler handler;

    public static void startActivity(Context context) {
        Intent intent = new Intent();
        intent.setClass(context, MainActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void initView() {
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                tvGo.setTextColor(RandomUtils.getRandownColor());
//                super.handleMessage(msg);
                handler.sendEmptyMessageDelayed(1, 1000);
            }
        };

        swipeRefreshLayout.setOnRefreshListener(() -> {
            handler.removeMessages(1);
            handler.sendEmptyMessage(1);
            swipeRefreshLayout.setRefreshing(false);
        });
    }

    @Override
    protected void initData() {

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    public void onBackPressed() {
        if (System.currentTimeMillis() - lastPressBackKeyTime < DELAY_TIME) {
            super.onBackPressed();
        } else {
            lastPressBackKeyTime = System.currentTimeMillis();
            ToastUtils.showLongMsg(getResources().getString(R.string.toast_press_again_exit_app));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventLogin(LoginEvent event) {
        if (event.isLogin()) {
            LogX.i("### 登录成功");
        } else {
            LogX.i("### 退出登录成功");
        }
    }

    @OnClick(R.id.tv_go)
    void onClick() {
        startActivity(new Intent(this, TestMainActivity.class));
        finish();
    }

    @Override
    public void onDestroy() {
        if (handler != null) {
            handler.removeMessages(1);
            handler.removeCallbacksAndMessages(null);
        }
        super.onDestroy();
    }
}
