package com.xmqian.app.project.ui.common.model;

import com.xmqian.app.project.ui.common.contract.MyWebViewContract;

public class MyWebViewModel implements MyWebViewContract.Model {
    private String ID;
    private String Title;
    private String Contents;

    public String getID() {
        return ID;
    }

    public String getTitle() {
        return Title;
    }

    public String getContents() {
        return Contents;
    }
}
