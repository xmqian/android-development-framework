package com.xmqian.app.project.ui.user.contract;

import com.example.common.base.mvp.BaseView;
import com.xmqian.app.project.ui.user.model.LoginModel;

public interface LoginContract {
    interface Model {
    }

    interface View extends BaseView {
        void onLoginSucceed(LoginModel data);

        void onRequestFailed(String message);

        void getCodeSuccess();
    }

    interface Presenter {
        /**
         * 登录
         *
         * @param username 用户名
         * @param pwd 密码
         */
        void doLogin(String username, String pwd);

        /**
         * 快捷登录
         *
         * @param username
         * @param smsCode
         */
        void quickLogin(String username, String smsCode);

        /**
         * 获取验证码
         *
         * @param type
         * @param phone
         */
        void getSmsCode2(int type, String phone);
    }
}
