package com.xmqian.app.project.ui.user.presenter;


import com.example.common.base.mvp.BasePresenter;
import com.xmqian.app.project.ui.user.contract.RegisterContract;

/**
 * Created by xuyougen on 2018/6/5.
 */

public class RegisterPresenter extends BasePresenter<RegisterContract.View> implements RegisterContract.Presenter {

    public RegisterPresenter(RegisterContract.View mView) {
        super(mView);
    }

    public void register(String mobile, String pwd, String code) {

    }

    public void doLogin(String username, String pwd) {

    }
}
