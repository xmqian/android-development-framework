package com.coszero.utilslibrary.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.coszero.utilslibrary.callback.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;


/**
 * @param <T>
 * @author xmqian
 * ClassName: BaseRecyclerAdapter2<p>
 * Author:oubowu<p>
 * Fuction: RecyclerView通用适配器<p>
 * 使用空布局时，需要传入空布局,getEmptyLayoutId()方法返回0为不使用空布局
 * CreateDate:2016/2/16 22:47<p>
 * UpdateUser:<p>
 * UpdateDate:<p>
 * @version 1.0.1
 * @version 1
 */
public abstract class BaseRecyclerAdapter2<T> extends BaseRecyclerAdapter<BaseRecyclerViewHolder> {


    public static final int TYPE_ITEM = 0;
    public static final int TYPE_FOOTER = 1;
    public static final int TYPE_EMPTY = 2;

    protected List<T> mData;
    protected boolean mUseAnimation;
    protected LayoutInflater mInflater;
    protected OnItemClickListener mClickListener;
    protected boolean mShowFooter;
    protected int mStatus = 0;

    private RecyclerView.LayoutManager mLayoutManager;

    private int mLastPosition = -1;

    public BaseRecyclerAdapter2(Context context, List<T> data) {
        this(context, data, true);
    }

    public BaseRecyclerAdapter2(Context context, List<T> data, boolean useAnimation) {
        this(context, data, useAnimation, null, 0);
    }

    public BaseRecyclerAdapter2(Context context, List<T> data, int status) {
        this(context, data, true, null, status);
    }

    public BaseRecyclerAdapter2(Context context, List<T> data,
                                boolean useAnimation,
                                RecyclerView.LayoutManager layoutManager, int status) {
        super(context, data);
        mContext = context;
        mUseAnimation = useAnimation;
        mLayoutManager = layoutManager;
        mData = data == null ? new ArrayList<T>() : data;
        mInflater = LayoutInflater.from(context);
        mStatus = status;
    }

    /**
     * @return 空布局, 可选方法，如果不需要可以不实现
     */
    protected int getEmptyLayoutId() {
        return 0;
    }

    @Override
    public BaseRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_FOOTER) {
            return new BaseRecyclerViewHolder(mContext,
                    mInflater.inflate(getFooterLayoutId(), parent, false));
        } else if (viewType == TYPE_EMPTY) {
            return new BaseRecyclerViewHolder(mContext, mInflater.inflate(getEmptyLayoutId(), parent, false));
        } else {
            final BaseRecyclerViewHolder holder = new BaseRecyclerViewHolder(mContext,
                    mInflater.inflate(getItemLayoutId(viewType), parent, false));
            if (mClickListener != null) {
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mClickListener.onItemClick(v, holder.getLayoutPosition(), holder);
                    }
                });
            }
            return holder;
        }
    }

    /**
     * @return 设置底部布局
     */
    protected int getFooterLayoutId() {
        return 0;
    }

    @Override
    public void onBindViewHolder(BaseRecyclerViewHolder holder, int position) {
        if (getItemViewType(position) == TYPE_FOOTER) {
            if (mLayoutManager != null) {
                if (mLayoutManager instanceof StaggeredGridLayoutManager) {
                    if (((StaggeredGridLayoutManager) mLayoutManager).getSpanCount() != 1) {
                        StaggeredGridLayoutManager.LayoutParams params = (StaggeredGridLayoutManager.LayoutParams) holder.itemView
                                .getLayoutParams();
                        params.setFullSpan(true);
                    }
                } else if (mLayoutManager instanceof GridLayoutManager) {
                    if (((GridLayoutManager) mLayoutManager)
                            .getSpanCount() != 1 && ((GridLayoutManager) mLayoutManager)
                            .getSpanSizeLookup() instanceof GridLayoutManager.DefaultSpanSizeLookup) {
                        throw new RuntimeException("网格布局列数大于1时应该继承SpanSizeLookup时处理底部加载时布局占满一行。");
                    }
                }
            }
        } else if (getItemViewType(position) == TYPE_EMPTY) {
            setEmptyView(holder);
//            bindData(holder, position, null);
//            showEmpty();
        } else {
            bindData(holder, position, mData.get(position));
//            if (mUseAnimation) {
//                setAnimation(holder.itemView, position);
//            }
        }
    }

    /**
     * @param holder 可以对空布局的View进行操作
     */
    protected void setEmptyView(BaseRecyclerViewHolder holder) {

    }

    protected void setAnimation(View viewToAnimate, int position) {
//        if (position > mLastPosition) {
//            Animation animation = AnimationUtils
//                    .loadAnimation(viewToAnimate.getContext(), R.anim.item_bottom_in);
//            viewToAnimate.startAnimation(animation);
//            mLastPosition = position;
//        }
    }

    @Override
    public void onViewDetachedFromWindow(BaseRecyclerViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
//        if (mUseAnimation && holder.itemView.getAnimation() != null && holder.itemView
//                .getAnimation().hasStarted()) {
//            holder.itemView.clearAnimation();
//        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isEmptyLs(mData)) {
            return TYPE_EMPTY;
        }
        if (mShowFooter && getItemCount() - 1 == position) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        //是否需要显示底部
        int i = (getFooterLayoutId() != 0 && mShowFooter) ? 1 : 0;
        //判断数据是否为空,再判断是否设置了空布局
        return !isEmptyLs(mData) ? mData.size() + i : (getEmptyLayoutId() == 0 ? 0 : 1);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mClickListener = listener;
    }

    public abstract int getItemLayoutId(int viewType);

    public abstract void bindData(BaseRecyclerViewHolder holder, int position, T item);

    public void showFooter() {
        // KLog.e("Adapter显示尾部: " + getItemCount());
        notifyItemInserted(getItemCount());
        mShowFooter = true;
    }

    public void hideFooter() {
        // KLog.e("Adapter隐藏尾部:" + (getItemCount() - 1));
        notifyItemRemoved(getItemCount() - 1);
        mShowFooter = false;
    }
}
