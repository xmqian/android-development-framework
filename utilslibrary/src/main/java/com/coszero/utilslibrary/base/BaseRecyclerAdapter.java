package com.coszero.utilslibrary.base;

import android.content.Context;
import android.view.LayoutInflater;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

/**
 * @author xmqian
 * ListView或GrideView的Adapter父类
 * @version 1
 */
public abstract class BaseRecyclerAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {
    protected List list;
    protected Context context;
    protected LayoutInflater inflater;
    protected Context mContext;


    public BaseRecyclerAdapter(Context context, List list) {
        this.list = list;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.mContext = context;

    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    //替换数据源
    public void setData(List data) {
        this.list = data;
        notifyDataSetChanged();
    }

    public void refresh(List data) {
        if (data != null) {
            if (data.size() > 0) {
                if (list == data) { // 两个list 是同一个
                    notifyDataSetChanged();
                    return;
                } else {
                    list.clear();
                    list.addAll(data);
                }
            } else {
                list.clear();
            }
        } else {
            list.clear();
        }
        notifyDataSetChanged();
    }


    /**
     * 清理List集合
     */
    public void clean() {
        list.clear();
        notifyDataSetChanged();
    }

    /**
     * 添加集合
     *
     * @param list
     */
    public void addAll(List list) {
        if (list != null) {
            if (list.size() > 0) {
                if (this.list.equals(list)) { // 两个list 是同一个
                    notifyDataSetChanged();
                    return;
                } else {
                    this.list.addAll(list);
                }
            }
            notifyDataSetChanged();
        }
    }

    /**
     * 添加集合到指定位置
     *
     * @param list
     * @param position
     */
    public void addAll(List list, int position) {
        this.list.addAll(position, list);
        notifyDataSetChanged();
    }

    /**
     * 返回List集合
     *
     * @return
     */
    public List getList() {
        return list;
    }

    protected static <V> boolean isEmptyLs(List<V> list) {
        return null == list || list.size() <= 0;
    }
}
