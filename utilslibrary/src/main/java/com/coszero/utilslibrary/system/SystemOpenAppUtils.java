package com.coszero.utilslibrary.system;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.Nullable;

import com.coszero.utilslibrary.utils.LogX;

import java.net.URLDecoder;

/**
 * Desc： 用于打开系统的app
 * <p>
 * Author： xmqian
 * Email:xmqian93@163.com
 * Date: 2019/7/27 16:30
 * @version 1
 */
public class SystemOpenAppUtils {
    /**
     * 打开系统浏览器
     *
     * @param url "https://xxx"
     * @link 原文：https://blog.csdn.net/Aminy123/article/details/81510354
     */
    public static void openSystemBrowser(Activity activity, @Nullable String url) {
        if (url.contains("%3A")) {
            //异常字符转换
            url = URLDecoder.decode(url);
        }
        if (!url.startsWith("http") && !url.startsWith("https")) {
            //如来链接地址不是http或者https开头，则加上
            url = "https://" + url;
        }
        try {
            //###8.0以下
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            Uri content_url = Uri.parse(url);//要跳转的网页
            intent.setData(content_url);
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                intent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
            } else {
                //###8.0
                intent.setClassName("org.chromium.webview_shell", "org.chromium.webview_shell.WebViewBrowserActivity");
            }
            activity.startActivity(intent);
        } catch (Exception e) {
            LogX.e("OpenSystemAppUtils.openSystemBrowser", "跳转系统浏览器异常");
            e.printStackTrace();
        } finally {
            try {
                //先尝试跳转系统浏览器，实在跳转不了就让用户自己选
                Intent intent1 = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                activity.startActivity(intent1);
            } catch (Exception e) {
                LogX.e("OpenSystemAppUtils.openSystemBrowser", "跳转浏览器链接地址异常");
                e.printStackTrace();
            }
        }
    }

    /**
     * 打开系统相册
     *
     * @param activity
     * @param requestCode 请求码
     */
    public static void openSystemImage(Activity activity, int requestCode) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_PICK);
        intent.setType("image/*");
        activity.startActivityForResult(intent, 2);
    }
}
