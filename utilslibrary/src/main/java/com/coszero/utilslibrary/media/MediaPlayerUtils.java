package com.coszero.utilslibrary.media;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.MediaPlayer;
import android.util.Log;

/**
 * Desc： 音频播放工具类
 * 播放音频时有一定延迟,实时读较高的建议使用SoundPool
 *
 * @author xmqian
 * Email:xmqian93@163.com
 * Date: 2020/9/3 0003 13:49
 * <p>
 * MediaPlayerUtils.playAdudio(mActivity, R.raw.serror);
 * @version 1
 */
public class MediaPlayerUtils {
    static MediaPlayer mMediaPlayer;


    public static MediaPlayer getMediaPlayer() {
        if (null == mMediaPlayer) {
            mMediaPlayer = new MediaPlayer();
        }

        return mMediaPlayer;
    }


    /**
     * 播放音频
     */
    public static void playAudio(Context mContext, String fileName) {
        try {
            stopAudio();//如果正在播放就停止
            AssetManager assetManager = mContext.getAssets();
            AssetFileDescriptor afd = assetManager.openFd(fileName);
            MediaPlayer mediaPlayer = getMediaPlayer();
            mediaPlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            mediaPlayer.setLooping(false);//循环播放
            mediaPlayer.prepare();
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    mediaPlayer.start();
                }
            });
        } catch (Exception e) {
            Log.e("播放音频失败", "");
        }
    }

    public static void playAdudio(Context mContext, int rawId) {
        if (null != mMediaPlayer) {
            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.pause();
            }
            mMediaPlayer.release();
        }
        mMediaPlayer = MediaPlayer.create(mContext, rawId);
        mMediaPlayer.start();
    }


    /**
     * 停止播放音频
     */
    public static void stopAudio() {
        try {
            if (null != mMediaPlayer) {
                if (mMediaPlayer.isPlaying()) {
                    mMediaPlayer.pause();
                    mMediaPlayer.reset();
                    mMediaPlayer.stop();
                }
            }
        } catch (Exception e) {
            Log.e("stopAudio", e.getMessage());
        }
    }
}
