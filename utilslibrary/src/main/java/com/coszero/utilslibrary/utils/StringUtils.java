package com.coszero.utilslibrary.utils;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author xmqian
 * @date 2018/5/29
 * @desc 字符串操作类
 * <p>
 * 将格式化字符串的方法使用format开头，比如金钱保留两位小数 formatMoney()
 * 判断字符串是否符合标准的都使用 is开头，比如是否是手机号 isPhoneNumber()
 * 类型转换都是用to开头，传入的类型作为入参传入就行，比如字符数组转字符串 toString(String[] strArray);
 * @version 1
 */
public class StringUtils {
    // <editor-fold desc="格式化字符串" defaultstate="collapsed">

    /**
     * 格式化小数位数
     * 由于金钱比较敏感，这里不能进行四舍五入,保持直接截取
     * 位数
     *
     * @param value 需要格式化的小数
     * @param isRoundingMode 是否进行舍入模式 true:进行四舍五入 false:直接截取多余的数字
     * @param decimalDigits 需要保留的小数位数
     * @return 格式化的字符串
     */
    public static String formatDecimalDigits(Double value, boolean isRoundingMode, int decimalDigits) {
        StringBuilder builder = new StringBuilder("###.");
        for (int i = 0; i < decimalDigits; i++) {
            //使用0的时候，如果位数不足会补0，使用#不会补足位数
            builder.append("0");
        }
        DecimalFormat df = new DecimalFormat(builder.toString());
        if (!isRoundingMode) {
            df.setRoundingMode(RoundingMode.FLOOR);
        }
        return df.format(value);
    }

    /**
     * @param money 格式化金钱，保留两位小数，不进行四舍五入
     * @return formatMoney
     */
    public static String formatMoney(String money) {
        return formatDecimalDigits(Double.valueOf(money), false, 2);
    }

    /**
     * @return 获取最小两位的数字
     */
    public static String formatMineTwoNumber(int i) {
        if (i < 10) {
            return "0" + i;
        }
        return "" + i;
    }

    /**
     * 格式化手机号，中间4-7位号码隐藏
     *
     * @param phoneNum
     * @return
     */
    public static String formatPhone(String phoneNum) {
        return phoneNum.substring(0, 3).intern() + "****" + phoneNum.substring(7, 11).intern();
    }

    /**
     * 截取字符串
     *
     * @param data 源数据
     * @param charAt 截取的字符
     * @return
     */
    public static String[] formatSplitString(String data, String charAt) {
        if (data != null && data.contains(charAt)) {
            return data.split(charAt);
        }
        return null;
    }
    //</editor-fold>

    // <editor-fold desc="判断字符串是否符合条件" defaultstate="collapsed">

    /**
     * 验证18位身份证
     *
     * @param input The input.
     * @return {@code true}: yes<br>{@code false}: no
     */
    public static boolean isIDCard(final CharSequence input) {
        String regExp = "^[1-9]\\d{5}[1-9]\\d{3}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}([0-9Xx])$";
        Pattern p = Pattern.compile(regExp);
        Matcher m = p.matcher(input);
        return m.matches();
    }

    /**
     * 密码是否是字母和数字的组合
     *
     * @param pwd
     * @return
     */
    public static boolean isPwd(String pwd) {
        String regex = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{8,16}$";
        return pwd.matches(regex);
    }

    /**
     * 是否是标准的手机号码
     *
     * @param str
     * @return
     */
    public static boolean isPhone(String str) {
        String regExp = "^((13[0-9])|(15[^4])|(18[0-9])|(17[0-8])|(147))\\d{8}$";
        Pattern p = Pattern.compile(regExp);
        Matcher m = p.matcher(str);
        return m.matches();
    }

    /**
     * 判断给定字符串是否空白串。
     * 空白串是指由空格、制表符、回车符、换行符组成的字符串
     * 若输入字符串为null或空字符串，返回true
     *
     * @param input
     * @return boolean
     */
    public static boolean isEmpty(String input) {
        if (input == null || "".equals(input))
            return true;

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (c != ' ' && c != '\t' && c != '\r' && c != '\n') {
                return false;
            }
        }
        return true;
    }

    /**
     * 判断是不是一个合法的电子邮件地址
     *
     * @param email
     * @return
     */
    public static boolean isEmail(String email) {
        Pattern emailer = Pattern
                .compile("\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*");
        if (email == null || email.trim().length() == 0)
            return false;
        return emailer.matcher(email).matches();
    }
//</editor-fold>

    // <editor-fold desc="类型转换" defaultstate="collapsed">

    /**
     * 字符串转整数
     *
     * @param str
     * @param defValue
     * @return
     * @deprecated Integer.valueOf(str)
     */
    public static int toInt(String str, int defValue) {
        try {
            return Integer.parseInt(str);
        } catch (Exception e) {
        }
        return defValue;
    }

    /**
     * 把数组转换成字符拼接的字符串
     * 默认使用“,”进行拼接
     *
     * @param ss 数组
     * @param splitChar 传入分割的字符
     * @return
     */
    public static String toString(String[] ss, String splitChar) {
        StringBuffer s = new StringBuffer("");
        if (null != ss) {
            for (int i = 0; i < ss.length - 1; i++) {
                s
//                        .append("'")
                        .append(ss[i])
//                        .append("'")
                        .append(splitChar);
            }
            if (ss.length > 0) {
                s.append("'").append(ss[ss.length - 1]).append("'");
            }
        }
        return s.toString();
    }

    /**
     * 把数组转换成字符拼接的字符串
     * 默认使用“,”进行拼接
     *
     * @param ss 数组
     * @return
     */
    public static String arrayToString(String[] ss) {
        return toString(ss, ",");
    }

    /**
     * 对象转整数
     *
     * @param obj
     * @return 转换异常返回 0
     * @deprecated 系统的方法更方便
     */
    public static int toInt(Object obj) {
        if (obj == null)
            return 0;
        return toInt(obj.toString(), 0);
    }

    /**
     * 对象转整数
     *
     * @param obj
     * @return 转换异常返回 0
     * @deprecated
     */
    public static long toLong(String obj) {
        try {
            return Long.parseLong(obj);
        } catch (Exception e) {
        }
        return 0;
    }

    /**
     * 字符串转布尔值
     *
     * @param b
     * @return 转换异常返回 false
     * @deprecated
     */
    public static boolean toBool(String b) {
        try {
            return Boolean.parseBoolean(b);
        } catch (Exception e) {
        }
        return false;
    }


    /**
     * 将一个InputStream流转换成字符串
     *
     * @param is
     * @return
     */
    public static String toConvertString(InputStream is) {
        StringBuffer res = new StringBuffer();
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader read = new BufferedReader(isr);
        try {
            String line;
            line = read.readLine();
            while (line != null) {
                res.append(line);
                line = read.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != isr) {
                    isr.close();
                    isr.close();
                }
                if (null != read) {
                    read.close();
                    read = null;
                }
                if (null != is) {
                    is.close();
                    is = null;
                }
            } catch (IOException e) {
            }
        }
        return res.toString();
    }

    /**
     * 判断字符串是不是数字
     *
     * @param str
     * @return
     */
    public static boolean isNumeric(String str) {

        if (str.startsWith("0")) {
            return false;
        }

        Pattern pattern = Pattern.compile("[0-9]*");
        Matcher isNum = pattern.matcher(str);
        if (!isNum.matches()) {
            return false;
        }
        return true;
    }

    /**
     * null转为长度为0的字符串
     *
     * @param s 待转字符串
     * @return s为null转为长度为0字符串，否则不改变
     */
    public static String toLength0(String s) {
        return s == null ? "" : s;
    }

    //</editor-fold>
}
