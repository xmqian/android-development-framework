package com.coszero.utilslibrary;


import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
        String a = "012345678";
        System.out.println(a.substring(0, 5));
        System.out.println(a.length());
        System.out.println(a.substring(5));
    }

    /**
     * 正则使用
     */
    @Test
    public void matcherUse() {
        String s = "shhf8237%#*@jj889";
        String regEx = "[^a-zA-Z0-9]";  //只能输入字母或数字
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(s);
        String str = m.replaceAll("").trim();    //删掉不是字母或数字的字符
        System.out.println(str);
    }
}