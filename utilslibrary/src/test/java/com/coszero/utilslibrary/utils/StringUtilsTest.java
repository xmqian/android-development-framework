package com.coszero.utilslibrary.utils;

import junit.framework.TestCase;

public class StringUtilsTest extends TestCase {

    public void testFormatMoney() {
        System.out.println("格式化金钱2位小数：" + StringUtils.formatMoney("311111.679"));
        System.out.println("格式化5位小数：" + StringUtils.formatDecimalDigits(311111.679D, false, 5));
        System.out.println("格式化3位小数并四舍五入：" + StringUtils.formatDecimalDigits(311111.6795D, true, 3));
    }
}