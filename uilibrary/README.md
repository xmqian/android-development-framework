# UI工具类

## [Banner横幅滚动](https://gitee.com/xmqian/UtilsLibrary/wikis/BaseIndicatorBanner?sort_id=1528973)
- 代码来源：https://github.com/H07000223/FlycoBanner_Master
- com.coszero.uilibrary.banner

## [ratingbar —— 评分控件](https://gitee.com/xmqian/UtilsLibrary/wikis/CBRatingBar?sort_id=1635869)
- 代码来源：https://juejin.im/entry/59e235586fb9a0450a6661b8
- com.coszero.uilibrary.view.ratingbar.CBRatingBar
> 使用方式参考链接文档

## widget
- CircleImageView  圆形图片控件
- CircleTextView    圆形文字控件
- ContainsEmojiEditText  可输入表情的输入框
- CountDownTextView 验证码倒计时
- [LooperTextView 滚动文字，上下滚动的](https://gitee.com/xmqian/UtilsLibrary/wikis/LooperTextView?sort_id=1582173)
- NoScrollViewPager  不能左右滑动的ViewPager
- ProgressWebView 带加载进度条的WebView
- RoundImageView  圆角图片
- WrapContentHeightViewPager  包裹内容高度的ViewPager
- [FoldTextView —— 折叠和伸展文字](https://gitee.com/xmqian/UtilsLibrary/wikis/FoldTextView?sort_id=1635884)
