package com.coszero.uilibrary.dialog;

import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.coszero.uilibrary.R;

/**
 * 请求等待提示工具类
 * 只会在页面第一次请求时出现，需要再次出现，需要调用 reset()方法
 * Created by zhangfeng on 2015/12/8.
 * @version 1
 */
public class LoadingDialog extends Dialog {
    private ImageView progressView;
    private TextView contentText;
    //    private Animation loadAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.loading);
    private RotateAnimation loadAnimation;
    private String content = "";
    private boolean isFirst = true;

    public LoadingDialog(Context context) {
        super(context, R.style.loading_dialog_style);
        setContentView(R.layout.ui_dialog_layout_loading);
        progressView = findViewById(R.id.progressView);
        contentText = findViewById(R.id.contentText);
        loadAnimation = new RotateAnimation(0, 359, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        loadAnimation.setDuration(1000);
        loadAnimation.setInterpolator(new LinearInterpolator());
        loadAnimation.setRepeatCount(RotateAnimation.INFINITE);
    }

    /**
     * 获取展示的内容
     *
     * @return
     */
    public String getContent() {
        return content;
    }

    /**
     * 设置需要在等待时显示的文字
     *
     * @param content
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 显示等待提示
     */
    public void showPD() {
        if (!TextUtils.isEmpty(content)) {
            contentText.setText(content);
        }
        progressView.startAnimation(loadAnimation);
        if (isFirst) {
            super.show();
            isFirst = false;
        }
    }

    /**
     * 重置显示
     */
    public void reset() {
        isFirst = true;
    }

    @Override
    public void dismiss() {
        loadAnimation.cancel();
        super.dismiss();
    }

    /**
     * @param flag 是否外部点击可消失，true:可以，false：不消失
     */
    public void showCancelalbe(boolean flag) {
        setCancelable(flag);
        setCanceledOnTouchOutside(flag);
        show();
    }

    /**
     * 提示消失
     */
    public void dismissPD() {
        if (isShowing()) {
            progressView.clearAnimation();
            super.dismiss();
        }
    }
}
