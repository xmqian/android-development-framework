package com.coszero.uilibrary.banner.widget.LoopViewPager;

import android.content.Context;
import android.view.animation.Interpolator;
import android.widget.Scroller;

/**
 * Desc：
 *
 * @author xmqian
 * Email:xmqian93@163.com
 * Date: 2021/7/13 0013 10:34
 * @version 1
 */
public class FixedSpeedScroller extends Scroller {
    private int mScrollSpeed = 450;

    public FixedSpeedScroller(Context context) {
        super(context);
    }

    public FixedSpeedScroller(Context context, Interpolator interpolator, int scrollSpeed) {
        super(context, interpolator);
        this.mScrollSpeed = scrollSpeed;
    }

    @Override
    public void startScroll(int startX, int startY, int dx, int dy, int duration) {
        super.startScroll(startX, startY, dx, dy, this.mScrollSpeed);
    }
}
