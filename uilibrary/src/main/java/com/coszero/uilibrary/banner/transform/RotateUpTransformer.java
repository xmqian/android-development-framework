package com.coszero.uilibrary.banner.transform;

import android.view.View;

import androidx.viewpager.widget.ViewPager;

import com.nineoldandroids.view.ViewHelper;

/**
 * Desc：
 *
 * @author xmqian
 * Email:xmqian93@163.com
 * Date: 2021/7/13 0013 10:36
 * @version 1
 */
public class RotateUpTransformer implements ViewPager.PageTransformer {

	private static final float ROT_MOD = -15f;

	@Override
	public void transformPage(View page, float position) {
		final float width = page.getWidth();
		final float rotation = ROT_MOD * position;

		ViewHelper.setPivotX(page,width * 0.5f);
        ViewHelper.setPivotY(page,0f);
        ViewHelper.setTranslationX(page,0f);
        ViewHelper.setRotation(page,rotation);
	}
}
