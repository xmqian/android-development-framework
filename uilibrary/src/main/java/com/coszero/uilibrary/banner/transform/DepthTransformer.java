package com.coszero.uilibrary.banner.transform;

import android.view.View;

import androidx.viewpager.widget.ViewPager;

import com.nineoldandroids.view.ViewHelper;

/**
 * Desc：
 *
 * @author xmqian
 * Email:xmqian93@163.com
 * Date: 2021/7/13 0013 10:36
 * @version 1
 */
public class DepthTransformer implements ViewPager.PageTransformer {
    private static final float MIN_SCALE_DEPTH = 0.75f;

    @Override
    public void transformPage(View page, float position) {
        float alpha, scale, translationX;
        if (position > 0 && position < 1) {
            // moving to the right
            alpha = (1 - position);
            scale = MIN_SCALE_DEPTH + (1 - MIN_SCALE_DEPTH) * (1 - Math.abs(position));
            translationX = (page.getWidth() * -position);
        } else {
            // use default for all other cases
            alpha = 1;
            scale = 1;
            translationX = 0;
        }

        ViewHelper.setAlpha(page, alpha);
        ViewHelper.setTranslationX(page, translationX);
        ViewHelper.setScaleX(page, scale);
        ViewHelper.setScaleY(page, scale);
    }
}
