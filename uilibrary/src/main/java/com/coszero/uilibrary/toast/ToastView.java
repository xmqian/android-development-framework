package com.coszero.uilibrary.toast;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.coszero.utilslibrary.app.AppManager;

/**
 * Desc：
 *
 * @author xmqian
 * Email:xmqian93@163.com
 * Date: 2021/7/13 0013 10:34
 * @version 1
 */
public class ToastView extends AppManager {
    private static Toast toast;
    private static ToastView toastView;

    public static ToastView getInstance() {
        if (toastView == null) {
            toastView = new ToastView();
        }
        return toastView;
    }

    /**
     * 短时间显示吐司，并且重复点击上一层吐司消失
     * 需要先在App中调用init()
     *
     * @param msg
     */
    public void showMsg(String msg) {
        if (null == toast) {
            toast = new Toast(getAppManager().currentActivity());
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setGravity(0, 0, 0);
            LayoutInflater inflate = (LayoutInflater)
                    getAppManager().currentActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflate.inflate(getLayout(), null);
            toast.setView(v);
        } else {
            toast.cancel();
        }
        toast.show();
    }

    public View getView() {
        return null;
    }

    public int getLayout() {
        return 0;
    }
}
