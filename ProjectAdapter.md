# 项目版本适配
## API适配
- ~~6.0以后的运行时权限,隐私权限动态申请(PermissionRequestUtils)~~  
- ~~7.0相机拍摄获取URI文件路径适配(com.example.common.utils.TestUtils.takePhoto)~~
- []7.0多窗口模式适配
- ~~8.0通知渠道(NotificationChannelUtils)~~
- []9.0HTTP请求明文限制适配
- []9.0限制访问通话记录和电话号码
- ~~9.0前台服务限制(权限申请已配置在Manifast文件中)~~
- ~~10.0增加外部存储器申请(代码已在Manifast中配置android:requestLegacyExternalStorage="true")~~

## 屏幕适配
- [x] 全面屏，异常比例屏幕尺寸适配18.5:9
- [x] 在过渡界面SplashActivity进行“刘海屏”适配
- [ ] 9.0刘海屏适配

## Android Studio构建适配
- [x] 4.1版本更新grade版本到6.5