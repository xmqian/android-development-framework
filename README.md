# 安卓开发框架

> 项目从V1.0.0开始就使用==**AndroidX**==开发,不熟悉的请谨慎使用

#### 介绍
- 为了使开发新项目不用考虑API版本适配、机型适配、屏幕适配的问题；
- 为了使开发新项目不用考虑网络框架、图片加载框架、UI框架开发框架的选择；
- 为了使开发新项目不用考虑时间工具类、日志工具类、字符串工具类的封装；
- 为了使开发新项目不用考虑配置渠道包、分支创建规则、版本迭代规则的定义；

#### 软件架构

> 这里介绍该项目的构成以及目录和部分文件的作用

- app(示例代码项目,可以测试适配的成果,以及一些比较常用的界面效果)
- commonlib(可变式公共库，可根据不同项目更改库里的代码,实现自定义功能,适用于同类型的产品不同的项目)
- signkey(存储密钥的文件夹,最好是将密钥和密钥信息同时放里面,为了安全建议不要加入到版本控制上传,除非私有或内部仓库)
- uilibrary(存储通用的自定义View,一般不用更改,如果增加跟项目相关性比较高的自定义View,建议添加到commonlib库)
- utilslibrary(存储通用的工具库,这里都是使用原生API封装的工具类,如果不满足需求可进行修改或新增)
- config.gradle(项目全局配置文件,内部已关联此文件,可配置版本号、包名、依赖库版本等)
- gradle.properties(此项目的请求服务器连接配置的地方,使用时需要到主moudle的build.gradle中进行配置,使用BuildConfig获取)
- .gitignore(版本管理忽略文件,这里配置了不需要版本管理的文件,可根据需要自行修改)

#### 使用教程

> 这里介绍使用本框架之前需要关注的几个文档以及作用,然后可按需拷贝或直接在此项目中创建新moudle,依赖commomlib库

- ProjectAdapter.md API适配、机型适配、屏幕适配查看
- JarListDoc.md 集成的开发框架及使用方法查看
- FixLog.md 历史版本修复、增加功能查看
- 使用此项目的自定义密钥配置,需要在主module的目录下创建signing.properties文件,存储密钥的密码、别名、密钥路径等信息;
- 使用此项目部分功能可单独拷贝commonlib,uilibrary,utilslibrary到自己的项目,但build.gradle依赖config.gradle,根目录的build.gradle,如不拷贝需自行修改;
- key信息，在app目录中的signing.properties文件中，需要填写对应的信息，配置已经在app的build.graadle文件中配置好了
- 项目配置信息，统一存放在项目根目录的config.gradle文件中，包含有构建版本，兼容库版本，项目版本号的统一处理适配
- 项目中一些功能如需关闭，可以到**com.xmqian.app.common.config.AppControl**文件中进行关闭
- app文件夹中的build.gradle文件设置了混淆模式，默认关闭，增加了65536异常处理机制
- 项目包含颜色资源，和屏幕适配资源，使用时先查看values文件夹
- 已包含过渡界面，过渡界面风格为全屏，过渡图片存放在drawable-xhdpi文件夹中,文件名为splash_image.png,可以替换为自己的图片
其余的东西不用动，逻辑类为，SplashActivity
- 项目中已包含 **Toast**提示工具类，已在MyApplication中进行了初始化，使用直接调用 **ToastUtils.show(str)** 方法即可
- 已包含日志工具类 **LogX** ，在项目中使用时直接使用，**LogX.e()**,内部做了正式包不会打印日志的处理
- 包含SharedPreferences工具类**SharedPreUtils** 用来存储一些简单的数据类型，当然，最好是用来存储应用的设置信息，比如是否推送，
是否已登录等信息。存储文件可以被直接读取打开，所以不建议存储隐私数据
- app的设置数据保存在config.AppConfig文件中，详细规则内部有
- 项目中配置多个主页**(MainBottomTabActivity)** 底部菜单主页、**(MainActivity)**普通单个主页、**(TestMainActivity)** 测试主页,内部模块通过这个主页进行跳转

#### 新项目集成方法
1. 拷贝.gitignore文件到新项目根目录，拷贝完可进行一次提交，后面的步骤可能新增文件比较多，后悔删起来还是比较麻烦（如果不使用Git版本管理可忽略这一步）；
2. 拷贝config.gradle文件到新项目根目录，拷贝完记得将文件中applicationId的内容改为新项目的AppID(包名),其他配置项根据需要改动;
3. 拷贝根目录的build.gradle文件到新项目根目录，这一步很重要，关键代码是 apply from: "config.gradle" ，这句是引入config.gradle文件的，单独拷贝这句代码也可；
4. 在新项目中直接 Import Module 本项目的commonlib依赖，会依赖三个库commonlib,uilibrary,utilslibrary，作用可看文档中的说明部分，uilibrary可不用集成，其他两个是依赖关系必须集成；
5. 在新项目主module中依赖commonlib依赖库（只需要依赖一个就行，其它的两个是共享关系）；
6. 以下说明可选择性拷贝，不拷贝需要自行修改一些API版本不一致的错误,具体版本号查看新项目的config.gradle文件中的配置。
拷贝说明：将本项目app module中的build.gradle,signing.properties文件拷贝到新项目，将本项目根目录下gradle.properties文件进行新增拷贝（即只拷贝多的内容）或全覆盖，将本项目中的signkey目录拷贝到新项目根目录；至此新项目可正常运行了，也可以调用依赖库中的代码；
7. 水平较高的朋友前面的步骤都可以选择性拷贝，感觉前面步骤太麻烦的可直接在本项目中开发，文件作用及说明可以查看其它的文档；

#### 使用说明
##### 项目规范解析
- base/BaseActivity
> 只作为管理所有Activity的父类，不进行额外操作。增添功能视项目本身而定
- 使用EventBus方法时,订阅者方法使用 ==onEvent==开头，例如onEventLogin()

##### 第三方工具包集成规则
> 如果第三方的工具可以进行封装，就进行封装使用。工具类在utils目录下面建立对应的工具类目录。
- utils/PermissionRequestUtils
> 权限权限申请,进行权限申请，申请结果在对应Activity中使用onRequestPermissionsResult()方法接收
- utils.apiadapter.DisplayCutoutUtils
> 屏幕适配工具类
- utils.apiadapter.NotificationChannelUtils
> 通知栏消息工具类
- utils.glide.ImageLoad
> 图片加载
- utils.logger.LoggerUtil
> 日志打印工具类
- utils.GsonUtil
> Gson解析工具类
- utils.ImageCompressionUtil
> 图片压缩工具类
- utils.SelectImageUtils
> 图片选择工具类，有可能还会包含选择视频，视封装的第三方决定


##### 项目资源解析
- values/colors.xml
> 存储一些基础色调，除了主题色，里面不保存有明确目的的色彩值
- values/app_colors
> 存储带有明确目的的色彩值，比如标题，问题，色彩值从colors文件中取得
- values/styles.xml
> 设置项目主题，仅限于基于AppTheme.Base继承创建界面主题
- values/dimens.xml
> 单位适配,仅在项目中的布局文件使用,需要用到大小明确的单位时，新建app_dimens文件
- values/strings.xml
> 保存通用的字符串资源，比如网络错误提示等，如果需要按模块区分字符串，新建app_dimens文件
- drawable
> 此文件夹下只放置可以做成.9图的资源图片，或者矢量图片，和shap,select资源.
资源文件命名规则，一般有明确目的的使用对应类名或者功能名进行命名。如果目的不明确，比如，按钮，输入框背景，
这些比较普遍的，就用 简称_样式_颜色_作用 进行命名。比如 sp_cir_rec_red_btn，这是方形圆角红色按钮。

完整英文 | 简写
---- | ----
select | sl
shap | sp
circle | cir
rectangle | rec
buttom | btn
background | bg

##### 开启混淆后，需要在混淆文件中配置实体类的混淆规则
- 在混淆gson的时候，需要注意将-keep class com.xxx.xxx.bean.** { *; }修改成你自己的解析类所在的包．这就要求我们的解析类最好都放在一个包．
- 在混淆rxjava的时候，需要注意将自己处理请求返回的转换类如：public class HttpResult<T>，　序列化，如下：
  public class HttpResult<T>implements Serializable {．　　这里主要是序列化后，这个类就不会被混淆，也就不会导致网络返回后，rxjava无法解析．


#### 参与贡献

1.  项目版本命名采用语义化命名规则(版本格式：主版本号(可能导致与旧版不兼容).次版本号(优化功能或小部分不兼容).修订号(修复问题与旧版兼容)),建议[查看规则](https://semver.org/lang/zh-CN/)
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
