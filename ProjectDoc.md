# 项目文档

## 账户管理模块

````
graph LR
登录 --> 注册
````

## 项目基本模块集成
- 已集成界面，登录/注册/忘记密码(com.xmqian.app.project.ui.user),过渡界面(SplashActivity),引导界面(GuideActivity)
- 已配置好用户信息管理类**com.xmqian.app.common.config.user.UserManager**,配置好**Toast**
- 数据统计：友盟统计8.0，已集成自动页面统计Activity,手动Fragment统计，传入TAG(类文件名),可自行更改，工具类（com.example.common.utils.umeng.UMengAnalyticsUtils）

## 集成Kotlin开发模块
- 已集成基础开发包