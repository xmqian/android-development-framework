# 安卓开发框架

#### Description
为了使开发新项目不用考虑API版本适配、机型适配、屏幕适配的问题；
为了使开发新项目不用考虑网络框架、图片加载框架、UI框架开发框架的选择；
为了使开发新项目不用考虑时间工具类、日志工具类、字符串工具类的封装；
为了使开发新项目不用考虑配置渠道包、分支创建规则、版本迭代规则的定义；

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
